DROP DATABASE portaldoensino;
CREATE DATABASE portaldoensino;
USE portaldoensino;

CREATE TABLE usuario (
id bigint not null primary key auto_increment,
email varchar(255) not null,
senha varchar(255) not null,
nome varchar(255) not null,
sobrenome varchar(255),
avatar varchar(1000),
ativo boolean not null
);

CREATE TABLE usuario_amigo (
id bigint not null primary key auto_increment,
usuario_id bigint default null,
amigo_id bigint default null,
dataultimavisualizacao timestamp,
mensagensnaovisualizadas integer default 0,
CONSTRAINT fk_usuario_amigo_usuario foreign key (usuario_id) references usuario(id),
CONSTRAINT fk_usuario_amigo_amigo foreign key (amigo_id) references usuario(id)
);

CREATE TABLE usuario_seguidor (
id bigint not null primary key auto_increment,
usuario_id bigint default null,
seguidor_id bigint default null,
CONSTRAINT fk_usuario_seguidor_usuario foreign key (usuario_id) references usuario(id),
CONSTRAINT fk_usuario_seguidor_seguidor foreign key (seguidor_id) references usuario(id)
);

CREATE TABLE instituicao (
id bigint not null primary key auto_increment,
nome varchar(1000) not null,
descricao varchar(5000),
tipoava varchar(50) not null,
possuiava boolean,
tokenacesso varchar(50),
enderecoava varchar(50)
);

CREATE TABLE instituicao_administrador (
instituicao_id bigint not null,
administrador_id bigint not null,
CONSTRAINT fk_instituicao_administrador_instituicao foreign key (instituicao_id) references instituicao(id),
CONSTRAINT fk_instituicao_administrador_administrador foreign key (administrador_id) references usuario(id)
);

CREATE TABLE curso (
id bigint not null primary key auto_increment,
nome varchar(1000) not null,
descricao varchar(5000),
instituicao_id bigint,
identificadorcursoava varchar(100),
CONSTRAINT fk_curso_instituicao foreign key (instituicao_id) references instituicao(id)
);

CREATE TABLE curso_membro_ava (
id bigint not null primary key auto_increment,
curso_id bigint,
usuario_id bigint,
tipo varchar(50),
moodleid varchar(100),
moodlepassword varchar(10),
finalizado boolean,
CONSTRAINT fk_curso_membro_ava_curso foreign key (curso_id) references curso(id),
CONSTRAINT fk_curso_membro_ava_membro foreign key (usuario_id) references usuario(id)
);

create table professor_curso (
professor_id bigint,
curso_id bigint,
CONSTRAINT fk_professor_curso_professor foreign key (professor_id) references usuario(id),
CONSTRAINT fk_professor_curso_curso foreign key (curso_id) references curso(id)
);

create table aluno_curso (
aluno_id bigint,
curso_id bigint,
CONSTRAINT fk_aluno_curso_aluno foreign key (aluno_id) references usuario(id),
CONSTRAINT fk_aluno_curso_curso foreign key (curso_id) references curso(id)
);

CREATE TABLE grupo (
id bigint not null primary key auto_increment,
nome varchar(1000) not null,
descricao varchar(5000),
tipo varchar(30) not null
);

CREATE TABLE grupo_usuario (
grupo_id bigint not null,
usuario_id bigint not null,
CONSTRAINT fk_grupo_usario_grupo foreign key (grupo_id) references grupo(id),
CONSTRAINT fk_grupo_usario_usuario foreign key (usuario_id) references usuario(id)
);

CREATE TABLE grupo_administrador (
grupo_id bigint not null,
administrador_id bigint not null,
CONSTRAINT fk_grupo_administrador_grupo foreign key (grupo_id) references grupo(id),
CONSTRAINT fk_grupo_administrador_administrador foreign key (administrador_id) references usuario(id)
);

CREATE TABLE publicacao (
id bigint not null primary key auto_increment,
titulo varchar(255),
conteudo varchar(10000),
tipopublicacao varchar(30),
datapublicacao timestamp not null,
respostacorreta_id bigint,
publicacaopai_id bigint,
usuariocadastro_id bigint not null,
grupo_id bigint,
instituicao_id bigint,
curso_id bigint,
CONSTRAINT fk_publicacao_usuario foreign key (usuariocadastro_id) references usuario(id),
CONSTRAINT fk_publicacao_publicacao_pai foreign key (publicacaopai_id) references publicacao(id),
CONSTRAINT fk_publicacao_respostacorreta foreign key (respostacorreta_id) references publicacao(id),
CONSTRAINT fk_publicacao_grupo foreign key (grupo_id) references grupo(id),
CONSTRAINT fk_publicacao_instituicao foreign key (instituicao_id) references instituicao(id),
CONSTRAINT fk_publicacao_curso foreign key (curso_id) references curso(id)
);

CREATE TABLE publicacao_aprovar (
publicacao_id bigint not null,
usuario_id bigint not null,
CONSTRAINT fk_publicacao_aprovar_publicacao foreign key (publicacao_id) references publicacao(id),
CONSTRAINT fk_publicacao_aprovar_usuario foreign key (usuario_id) references usuario(id)
);

CREATE TABLE tema (
id bigint not null primary key auto_increment,
nome varchar (100) not null,
descricaocurta varchar(500),
descricaolonga varchar(5000)
);

CREATE TABLE grupo_tema (
grupo_id bigint not null,
tema_id bigint not null,
CONSTRAINT fk_grupo_tema_grupo foreign key (grupo_id) references grupo(id),
CONSTRAINT fk_grupo_tema_tema foreign key (tema_id) references tema(id)
);

CREATE TABLE tema_favorito (
tema_id bigint not null,
usuario_id bigint not null,
CONSTRAINT fk_tema_favorito_tema foreign key (tema_id) references tema(id),
CONSTRAINT fk_tema_favorito_usuario foreign key (usuario_id) references usuario(id)
);

CREATE TABLE instituicao_seguida (
instituicao_id bigint not null,
usuario_id bigint not null,
CONSTRAINT fk_instituicao_seguida_instituicao foreign key (instituicao_id) references instituicao(id),
CONSTRAINT fk_intituicao_seguida_usuario foreign key (usuario_id) references usuario(id)
);

CREATE TABLE publicacao_tema(
publicacao_id bigint not null,
tema_id bigint not null,
CONSTRAINT fk_publicacao_tema_publicacao foreign key (publicacao_id) references publicacao(id),
CONSTRAINT fk_publicacao_tema_tema foreign key (tema_id) references tema(id)
);

CREATE TABLE notificacao (
id bigint not null primary key auto_increment,
descricao varchar(1000) not null,
usuarionotificado_id bigint,
usuarioenvio_id bigint,
grupo_id bigint,
instituicao_id bigint,
curso_id bigint,
finalizado boolean not null,
notificacaoaceita boolean,
notificacaotipo varchar(50),
CONSTRAINT fk_notificacao_usuario_envio foreign key (usuarioenvio_id) references usuario(id),
CONSTRAINT fk_notificacao_usuario_notificado foreign key (usuarionotificado_id) references usuario(id),
CONSTRAINT fk_notificacao_grupo foreign key (grupo_id) references grupo(id),
CONSTRAINT fk_notificacao_instituicao foreign key (instituicao_id) references instituicao(id),
CONSTRAINT fk_notificacao_curso foreign key (curso_id) references curso(id)
);

CREATE TABLE mensagemchat (
id bigint not null primary key auto_increment,
dataenvio date not null ,
mensagem varchar(1000),
usuarioenvio_id bigint default null,
usuariorecebimento_id bigint default null,
CONSTRAINT fk_mensagemchat_usuarioenvio foreign key (usuarioenvio_id) references usuario(id),
CONSTRAINT fk_mensagemchat_usuariorecebimento foreign key (usuariorecebimento_id) references usuario(id)
);

CREATE TABLE mensagem (
id bigint not null primary key auto_increment,
dataenvio timestamp not null ,
titulo varchar(255),
mensagem varchar(5000),
visualizado boolean,
mensagempai_id bigint default null,
usuarioenvio_id bigint default null,
usuariorecebimento_id bigint default null,
CONSTRAINT fk_mensagem_mensagem foreign key (mensagempai_id) references mensagem(id),
CONSTRAINT fk_mensagem_usuarioenvio foreign key (usuarioenvio_id) references usuario(id),
CONSTRAINT fk_mensagem_usuariorecebimento foreign key (usuariorecebimento_id) references usuario(id)
);

CREATE TABLE cursomembroava(
id bigint not null primary key auto_increment,
tipo varchar(50),
moodleId integer,
moodlepassword varchar(50),
finalizado boolean,
curso_id bigint default null,
usuario_id bigint default null,
CONSTRAINT fk_cursomembroava_curso foreign key (curso_id) references curso(id),
CONSTRAINT fk_cursomembroava_usuario foreign key (usuario_id) references usuario(id)
);

ALTER TABLE grupo_usuario ADD UNIQUE membro_unico (grupo_id, usuario_id);
ALTER TABLE grupo_administrador ADD UNIQUE administrado_unico (grupo_id, administrador_id);
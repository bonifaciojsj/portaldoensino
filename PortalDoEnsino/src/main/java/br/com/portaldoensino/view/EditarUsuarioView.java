package br.com.portaldoensino.view;

import br.com.portaldoensino.entity.Usuario;
import br.com.portaldoensino.service.UsuarioService;
import br.com.portaldoensino.sessao.SessionController;
import br.com.portaldoensino.util.ArquivoUtil;
import br.com.portaldoensino.view.util.FacesNavigationUtil;
import java.io.IOException;
import java.io.Serializable;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class EditarUsuarioView implements Serializable {

    @Inject
    private SessionController sessionController;
    @Inject
    private UsuarioService usuarioService;
    private Usuario usuario;
    private Long id;

    public Usuario getUsuario() {
        if (usuario == null) {
            usuario = sessionController.getUsuarioLogado();
        }
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String salvar() {
        usuario = usuarioService.salvar(usuario);
        sessionController.setUsuarioLogado(usuario);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT;
    }

    public String cancelarAlteracoes() {
        usuario = usuarioService.buscarPorId(usuario.getId());
        sessionController.setUsuarioLogado(usuario);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT;
    }

    public void uploadAvatar(FileUploadEvent event) {
        try {
            usuario.setAvatar(ArquivoUtil.salvarArquivo("/Usuarios/" + getUsuario().getId() + "/",
                    event.getFile().getFileName(),
                    event.getFile().getInputstream()));
        } catch (IOException e) {

        }
    }
    
}

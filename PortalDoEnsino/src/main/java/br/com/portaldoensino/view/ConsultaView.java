package br.com.portaldoensino.view;

import br.com.portaldoensino.entity.Curso;
import br.com.portaldoensino.entity.Grupo;
import br.com.portaldoensino.entity.Instituicao;
import br.com.portaldoensino.entity.Publicacao;
import br.com.portaldoensino.entity.Tema;
import br.com.portaldoensino.entity.Usuario;
import br.com.portaldoensino.service.GrupoService;
import br.com.portaldoensino.service.InstituicaoService;
import br.com.portaldoensino.service.PublicacaoService;
import br.com.portaldoensino.service.TemaService;
import br.com.portaldoensino.service.UsuarioService;
import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author BONIFACIO
 */
@Named
@ViewScoped
public class ConsultaView implements Serializable {
    
    @Inject
    private TemaService temaService;
    @Inject
    private UsuarioService usuarioService;
    @Inject
    private PublicacaoService publicacaoService;
    @Inject
    private GrupoService grupoService;
    @Inject
    private InstituicaoService instituicaoService;
    
    private String stringConsulta;
    private List<Usuario> usuarios;
    private List<Tema> temas;
    private List<Publicacao> publicacoes;
    private List<Grupo> grupos;
    private List<Instituicao> instituicoes;

    public String getStringConsulta() {
        return stringConsulta;
    }

    public void setStringConsulta(String stringConsulta) {
        this.stringConsulta = stringConsulta;
    }

    public List<Tema> getTemas() {
        if (temas == null) {
            temas = temaService.buscarTemasPorNome(stringConsulta);
        }
        return temas;
    }

    public void setTemas(List<Tema> temas) {
        this.temas = temas;
    }

    public List<Usuario> getUsuarios() {
        if (usuarios == null) {
            usuarios = usuarioService.buscarUsuariosPorNome(stringConsulta);
        }
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public List<Publicacao> getPublicacoes() {
        if (publicacoes == null) {
            publicacoes = publicacaoService.buscarPublicacoesPorString(stringConsulta);
        }
        return publicacoes;
    }

    public void setPublicacoes(List<Publicacao> publicacoes) {
        this.publicacoes = publicacoes;
    }

    public List<Grupo> getGrupos() {
        if (grupos == null) {
            grupos = grupoService.buscarPorString(stringConsulta);
        }
        return grupos;
    }

    public void setGrupos(List<Grupo> grupos) {
        this.grupos = grupos;
    }

    public List<Instituicao> getInstituicoes() {
        if (instituicoes == null) {
            instituicoes = instituicaoService.buscarPorString(stringConsulta);
        }
        return instituicoes;
    }

    public void setInstituicoes(List<Instituicao> instituicoes) {
        this.instituicoes = instituicoes;
    }
    
}

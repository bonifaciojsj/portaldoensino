package br.com.portaldoensino.view;

import br.com.portaldoensino.entity.Grupo;
import br.com.portaldoensino.entity.GrupoTipo;
import br.com.portaldoensino.entity.Tema;
import br.com.portaldoensino.entity.Usuario;
import br.com.portaldoensino.service.GrupoService;
import br.com.portaldoensino.service.NotificacaoService;
import br.com.portaldoensino.service.TemaService;
import br.com.portaldoensino.service.UsuarioService;
import br.com.portaldoensino.sessao.SessionController;
import br.com.portaldoensino.view.util.FacesNavigationUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author BONIFACIO
 */
@Named
@ViewScoped
public class GrupoView implements Serializable {

    @Inject
    private SessionController session;
    @Inject
    private GrupoService grupoService;
    @Inject
    private NotificacaoService notificacaoService;
    @Inject
    private TemaService temaService;
    @Inject
    private UsuarioService usuarioService;
    private Grupo grupo;
    private Usuario usuarioConvidado;

    public Grupo getGrupo() {
        if (grupo == null) {
            grupo = new Grupo();
        }
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public Usuario getUsuarioConvidado() {
        if (usuarioConvidado == null) {
            usuarioConvidado = new Usuario();
        }
        return usuarioConvidado;
    }

    public void setUsuarioConvidado(Usuario usuarioConvidado) {
        this.usuarioConvidado = usuarioConvidado;
    }
    
    public List<Usuario> getUsuarios(String query) {
        List<Usuario> usuarios = usuarioService.buscarUsuariosNaoMembrosENaoPendentesPorGrupo(grupo);
        List<Usuario> usuariosFiltrados = new ArrayList<>();
        for (Usuario usuario : usuarios) {
            if (usuario.getNomeCompleto().toLowerCase().contains(query)) {
                usuariosFiltrados.add(usuario);
            }
        }
        return usuariosFiltrados;
    }    

    public void load() {
        this.grupo = grupoService.buscarPorId(grupo.getId());
    }

    public void loadEdicao() {
        if (session.isLoggedIn()) {
            this.grupo = grupoService.buscarPorId(grupo.getId());
            if (!grupo.getAdministradores().contains(session.getUsuarioLogado())) {
                grupo = null;
            }
        }
    }

    public List<Tema> todosTemas(String query) {
        List<Tema> todosTemas = temaService.buscarTodos();
        List<Tema> temasDisponiveis = new ArrayList<>();
        for (Tema tema : todosTemas) {
            tema.setNome(tema.getNome().toLowerCase());
            query = query.toLowerCase();
            if (tema.getNome().contains(query)) {
                temasDisponiveis.add(tema);
            }
        }
        Tema novoTema = new Tema();
        novoTema.setNome(query);
        if (temaService.buscarPorNome(query) == null) {
            temasDisponiveis.add(novoTema);
        }
        return temasDisponiveis;
    }

    public GrupoTipo[] getTiposGrupo() {
        return GrupoTipo.values();
    }

    public String salvar() {
        for (Tema tema : grupo.getTemas()) {
            salvarTema(tema);
        }
        if (grupo.isNew()) {
            grupo.setAdministradores(new ArrayList<Usuario>());
            grupo.setMembros(new ArrayList<Usuario>());
            grupo.getAdministradores().add(session.getUsuarioLogado());
            grupo.getMembros().add(session.getUsuarioLogado());
        }
        session.setGruposParticipados(null);
        grupo = grupoService.salvar(grupo);
        return "grupo.xhtml" + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public void salvarTema(Tema tema) {
        if (tema.isNew()) {
            Character.toUpperCase(tema.getNome().charAt(0));
            temaService.salvar(tema);
        }
    }

    public String cancelarAlteracoes() {
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public String editarGrupo() {
        return "incluirEditarGrupo.xhtml" + FacesNavigationUtil.FACES_REDIRECT + "&grupoId=" + grupo.getId();
    }

    public String deixarGrupo() {
        if (grupo.getAdministradores().contains(session.getUsuarioLogado())) {
            grupo.getAdministradores().remove(session.getUsuarioLogado());
        }
        grupo.getMembros().remove(session.getUsuarioLogado());
        grupo = grupoService.salvar(grupo);
        session.setGruposParticipados(null);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public String voltarParaGrupo() {
        return "grupo.xhtml" + FacesNavigationUtil.FACES_REDIRECT + "&grupoId=" + grupo.getId();
    }
    
    public String administrarUsuarios() {
        return "administrarUsuarios.xhtml" + FacesNavigationUtil.FACES_REDIRECT + "&grupoId=" + grupo.getId();        
    }
    
    public String solicitarParticipacao() {
        notificacaoService.gerarNotificacaoDeSoliciticaoDeIngressoParaGrupo(session.getUsuarioLogado(), grupo);        
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public String adicionarAdministrador(Usuario usuario) {
        notificacaoService.gerarNotificacaoDeConviteParaAdministradorDeGrupo(usuario, grupo);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public String removerAdministrador(Usuario usuario) {
        notificacaoService.gerarNotificacaoDeRemocaoDeAdministradorDeGrupo(usuario, grupo);
        grupo.getAdministradores().remove(usuario);
        grupo = grupoService.salvar(grupo);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public String removerDoGrupo(Usuario usuario) {
        notificacaoService.gerarNotificacaoDeRemocaoDeMembroDeGrupo(usuario, grupo);
        grupo.getMembros().remove(usuario);
        if (grupo.getAdministradores().contains(usuario)) {
            grupo.getAdministradores().remove(usuario);
        }
        grupo = grupoService.salvar(grupo);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public boolean isRenderizarGrupoPrivado() {
        return grupo.isPrivado()
                && (!session.isLoggedIn() || !grupo.getMembros().contains(session.getUsuarioLogado()));
    }

    public boolean isRenderizarPublicacoes() {
        return grupo.isPublico()
                || (grupo.isPrivado() && session.isLoggedIn() && grupo.getMembros().contains(session.getUsuarioLogado()));
    }
    
    public boolean possuiSolicitacaoEntradaPendente() {
        return grupoService.possuiSolicitacaoDeIngressoPendente(session.getUsuarioLogado(), grupo);
    }
    
    public boolean possuiConviteParaAdministradorPendente(Usuario usuario) {
        return grupoService.possuiConviteDeAdministradorPendente(usuario, grupo);
    }
    
    public String convidarUsuario() {
        notificacaoService.gerarNotificacaoDeConviteParaMembroDeGrupo(usuarioConvidado, grupo);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }
}

package br.com.portaldoensino.view;

import br.com.portaldoensino.entity.Mensagem;
import br.com.portaldoensino.entity.MensagemPorMensagemPai;
import br.com.portaldoensino.entity.Usuario;
import br.com.portaldoensino.service.MensagemService;
import br.com.portaldoensino.service.UsuarioService;
import br.com.portaldoensino.sessao.SessionController;
import br.com.portaldoensino.view.util.FacesNavigationUtil;
import java.io.Serializable;
import java.util.*;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class MensagemView implements Serializable {

    @Inject
    private SessionController session;
    @Inject
    private MensagemService mensagemService;
    @Inject
    private UsuarioService usuarioService;
    private List<MensagemPorMensagemPai> mensagensRecebidas;
    private List<Mensagem> mensagens;
    private Mensagem mensagem;
    private Mensagem mensagemPai;
    private Usuario usuarioRecebimento;

    public List<MensagemPorMensagemPai> getMensagensRecebidas() {
        if (mensagensRecebidas == null) {
            mensagensRecebidas = new ArrayList<>();
            List<Mensagem> mensagens = mensagemService.buscarTodosPorUsuario(session.getUsuarioLogado());
            Map<Mensagem, List<Mensagem>> mapaMensagens = new HashMap<>();
            for (Mensagem mensagem : mensagens) {
                if (mensagem.getMensagemPai() == null
                        && (!mapaMensagens.containsKey(mensagem) || !mapaMensagens.containsKey(mensagem.getMensagemPai()))) {
                    List<Mensagem> mensagensDependentes = new ArrayList<>();
                    mensagensDependentes.add(mensagem);
                    mapaMensagens.put(mensagem, mensagensDependentes);
                } else {
                    List<Mensagem> mensagensDependentes = mapaMensagens.get(mensagem.getMensagemPai());
                    mensagensDependentes.add(mensagem);
                    mapaMensagens.put(mensagem.getMensagemPai(), mensagensDependentes);
                }
            }
            for (Map.Entry<Mensagem, List<Mensagem>> mapa : mapaMensagens.entrySet()) {
                Mensagem mensagemPai = new Mensagem();
                int mensagensNaoLidas = 0;
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(0);
                cal.set(1900, 10, 10, 0, 0, 0);
                Date ultimaMensagemEnviada = cal.getTime();
                for (Mensagem mensagem : mapa.getValue()) {
                    mensagemPai = mensagem.getMensagemPai() == null ? mensagem : mensagem.getMensagemPai();
                    if (!mensagem.getVisualizado() && session.getUsuarioLogado().getId().equals(mensagem.getUsuarioRecebimento().getId())) {
                        mensagensNaoLidas++;
                    }
                    if (mensagem.getDataEnvio().after(ultimaMensagemEnviada)) {
                        ultimaMensagemEnviada = mensagem.getDataEnvio();
                    }
                }
                mensagensRecebidas.add(new MensagemPorMensagemPai(mensagemPai, mensagensNaoLidas, new java.sql.Timestamp(ultimaMensagemEnviada.getTime())));
            }

            Collections.sort(mensagensRecebidas, new Comparator<MensagemPorMensagemPai>() {
                @Override
                public int compare(MensagemPorMensagemPai o1, MensagemPorMensagemPai o2) {
                    int ordem = ((Integer) o2.getMensagensNaoLidas()).compareTo(((Integer) o1.getMensagensNaoLidas()));
                    if (ordem == 0) {
                        ordem = o2.getUltimaMensagemEnviada().compareTo(o1.getUltimaMensagemEnviada());
                    }
                    return ordem;
                }
            });
        }
        return mensagensRecebidas;
    }

    public void setMensagensRecebidas(List<MensagemPorMensagemPai> mensagensRecebidas) {
        this.mensagensRecebidas = mensagensRecebidas;
    }

    public List<Mensagem> getMensagens() {
        if (mensagens == null) {
            mensagens = new ArrayList<>();
        }
        return mensagens;
    }

    public void setMensagens(List<Mensagem> mensagens) {
        this.mensagens = mensagens;
    }

    public Mensagem getMensagemPai() {
        if (mensagemPai == null) {
            mensagemPai = new Mensagem();
        }
        return mensagemPai;
    }

    public void setMensagemPai(Mensagem mensagemPai) {
        this.mensagemPai = mensagemPai;
    }

    public Mensagem getMensagem() {
        if (mensagem == null) {
            mensagem = new Mensagem();
        }
        return mensagem;
    }

    public void setMensagem(Mensagem mensagem) {
        this.mensagem = mensagem;
    }

    public List<Usuario> getUsuarios(String query) {
        List<Usuario> usuarios = usuarioService.buscarTodos();
        List<Usuario> usuariosFiltrados = new ArrayList<>();
        for (Usuario usuario : usuarios) {
            if (usuario.getNomeCompleto().toLowerCase().contains(query)) {
                usuariosFiltrados.add(usuario);
            }
        }
        return usuariosFiltrados;
    }

    public Usuario getUsuarioRecebimento() {
        if (usuarioRecebimento == null) {
            usuarioRecebimento = new Usuario();
        }
        return usuarioRecebimento;
    }

    public void setUsuarioRecebimento(Usuario usuarioRecebimento) {
        this.usuarioRecebimento = usuarioRecebimento;
    }

    public void carregarUsuarioRecebimento() {
        usuarioRecebimento = usuarioService.buscarPorId(getUsuarioRecebimento().getId());
    }

    public void carregarMensagens() {
        mensagemPai = mensagemService.buscarPorId(getMensagemPai().getId());
        if (mensagemPai != null) {
            mensagens = mensagemService.buscarTodosDeMensagemPai(getMensagemPai());
            mensagemService.atualizarMensagensNaoLidas(getMensagemPai(), session.getUsuarioLogado());
        }
    }

    public String visualizarMensagens(Mensagem mensagem) {
        return "mensagem.xhtml" + FacesNavigationUtil.FACES_REDIRECT + "&mensagemId=" + mensagem.getId();
    }

    public String enviarMensagem() {
        mensagem.setUsuarioEnvio(session.getUsuarioLogado());
        if (usuarioRecebimento.isNew()) {
            if (getMensagemPai().getMensagemPai() == null) {
                mensagem.setMensagemPai(mensagemPai);
                if (mensagem.getUsuarioEnvio().equals(mensagemPai.getUsuarioRecebimento())) {
                    mensagem.setUsuarioRecebimento(mensagemPai.getUsuarioEnvio());
                } else {
                    mensagem.setUsuarioRecebimento(mensagemPai.getUsuarioRecebimento());
                }
            }
        } else {
            mensagem.setUsuarioRecebimento(usuarioRecebimento);
        }
        mensagem.setVisualizado(false);
        mensagem.setDataEnvio(new Date());
        mensagem = mensagemService.salvar(mensagem);
        if (mensagem.getMensagemPai() == null) {
            mensagemPai = mensagem;
            usuarioRecebimento = null;
        }
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }
    
    public String novaMensagem() {
        return "mensagem.xhtml" + FacesNavigationUtil.FACES_REDIRECT;
    }

}

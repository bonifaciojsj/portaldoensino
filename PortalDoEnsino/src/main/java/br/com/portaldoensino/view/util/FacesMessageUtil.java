package br.com.portaldoensino.view.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author jose.bonifacio
 */
public abstract class FacesMessageUtil {

    public static void addErrorMessage(String mensagem, boolean manterAposRedirecionamento) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, mensagem, null));
        setarSeMensagemSeriaMantidaAposRedireciomamento(manterAposRedirecionamento);
    }

    public static void addInfoMessage(String mensagem, boolean manterAposRedirecionamento) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, mensagem, null));
        setarSeMensagemSeriaMantidaAposRedireciomamento(manterAposRedirecionamento);
    }

    private static void setarSeMensagemSeriaMantidaAposRedireciomamento(boolean manterAposRedirecionamento) {
        if (manterAposRedirecionamento) {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        }
    }
}

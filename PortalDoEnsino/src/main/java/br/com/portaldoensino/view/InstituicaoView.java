package br.com.portaldoensino.view;

import br.com.portaldoensino.entity.Curso;
import br.com.portaldoensino.entity.CursoMembroAvaTipo;
import br.com.portaldoensino.entity.Instituicao;
import br.com.portaldoensino.entity.InstituicaoSeguida;
import br.com.portaldoensino.entity.InstituicaoSeguidaPK;
import br.com.portaldoensino.entity.InstituicaoTipoAva;
import br.com.portaldoensino.entity.Usuario;
import br.com.portaldoensino.service.CursoService;
import br.com.portaldoensino.service.InstituicaoService;
import br.com.portaldoensino.service.NotificacaoService;
import br.com.portaldoensino.service.UsuarioService;
import br.com.portaldoensino.sessao.SessionController;
import br.com.portaldoensino.view.util.FacesNavigationUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.context.RequestContext;

/**
 *
 * @author BONIFACIO
 */
@Named
@ViewScoped
public class InstituicaoView implements Serializable {

    @Inject
    private SessionController session;
    @Inject
    private InstituicaoService instituicaoService;
    @Inject
    private CursoService cursoService;
    @Inject
    private NotificacaoService notificacaoService;
    @Inject
    private UsuarioService usuarioService;
    private Instituicao instituicao;
    private boolean instituicaoSeguida;
    private String novoCurso;
    private Curso curso;
    private Usuario usuarioConvidado;
    private Usuario professorConvidado;

    public Instituicao getInstituicao() {
        if (instituicao == null) {
            instituicao = new Instituicao();
        }
        return instituicao;
    }

    public void setInstituicao(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    public void loadEdicao() {
        if (session.isLoggedIn()) {
            this.instituicao = instituicaoService.buscarPorId(getInstituicao().getId());
            if (!instituicao.getAdministradores().contains(session.getUsuarioLogado())) {
                instituicao = null;
            }
        }
    }
    
    public void loadEdicaoCurso() {
        if (session.isLoggedIn()) {
            this.curso = cursoService.buscarPorId(getCurso().getId());
            if (!curso.getInstituicao().getAdministradores().contains(session.getUsuarioLogado())) {
                curso = null;
            }
        }
    }

    public void loadCurso() {
        this.curso = cursoService.buscarPorId(getCurso().getId());
        this.instituicao = instituicaoService.buscarPorId(curso.getInstituicao().getId());
    }

    public Usuario getUsuarioConvidado() {
        if (usuarioConvidado == null) {
            usuarioConvidado = new Usuario();
        }
        return usuarioConvidado;
    }

    public Usuario getProfessorConvidado() {
        if (professorConvidado == null) {
            professorConvidado = new Usuario();
        }
        return professorConvidado;
    }

    public void setProfessorConvidado(Usuario professorConvidado) {
        this.professorConvidado = professorConvidado;
    }    
    
    public void setUsuarioConvidado(Usuario usuarioConvidado) {
        this.usuarioConvidado = usuarioConvidado;
    }

    public List<Usuario> getUsuarios(String query) {
        List<Usuario> usuarios = usuarioService.buscarUsuariosNaoAdministradoresENaoPendentesPorInstituicao(instituicao);
        List<Usuario> usuariosFiltrados = new ArrayList<>();
        for (Usuario usuario : usuarios) {
            if (usuario.getNomeCompleto().toLowerCase().contains(query)) {
                usuariosFiltrados.add(usuario);
            }
        }
        return usuariosFiltrados;
    }
    
    public List<Usuario> getPossiveisAlunos(String query) {
        List<Usuario> usuarios = usuarioService.buscarUsuariosNaoAlunosENaoPendentesPorCurso(curso);
        List<Usuario> usuariosFiltrados = new ArrayList<>();
        for (Usuario usuario : usuarios) {
            if (usuario.getNomeCompleto().toLowerCase().contains(query)) {
                usuariosFiltrados.add(usuario);
            }
        }
        return usuariosFiltrados;
    }
    
    public List<Usuario> getPossiveisProfessores(String query) {
        List<Usuario> usuarios = usuarioService.buscarUsuariosNaoProfessoresENaoPendentesPorCurso(curso);
        List<Usuario> usuariosFiltrados = new ArrayList<>();
        for (Usuario usuario : usuarios) {
            if (usuario.getNomeCompleto().toLowerCase().contains(query)) {
                usuariosFiltrados.add(usuario);
            }
        }
        return usuariosFiltrados;
    }
        
    public String convidarAdministrador() {
        notificacaoService.gerarNotificacaoDeConviteParaAdministradorDeInstituicao(usuarioConvidado, instituicao);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }
        
    public String convidarAluno() {
        notificacaoService.gerarNotificacaoDeConviteParaAlunoDeCurso(usuarioConvidado, curso);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }
        
    public String convidarProfessor() {
        notificacaoService.gerarNotificacaoDeConviteParaProfessorDeCurso(professorConvidado, curso);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public String voltarParaInstituicao() {
        return "instituicao.xhtml" + FacesNavigationUtil.FACES_REDIRECT + "&instituicaoId=" + instituicao.getId();
    }

    public String administrarUsuarios() {
        return "administrarUsuarios.xhtml" + FacesNavigationUtil.FACES_REDIRECT + "&instituicaoId=" + instituicao.getId();
    }

    public String administrarMembrosDoCurso() {
        return "administrarMembrosCurso.xhtml" + FacesNavigationUtil.FACES_REDIRECT + "&cursoId=" + curso.getId();
    }

    public String voltarParaCurso() {
        return "curso.xhtml" + FacesNavigationUtil.FACES_REDIRECT + "&cursoId=" + curso.getId();
    }

    public String deixarCurso() {
        if (curso.getProfessores().contains(session.getUsuarioLogado())) {
            curso.getProfessores().remove(session.getUsuarioLogado());
            curso.getProfessores().remove(session.getUsuarioLogado());
        }
        if (curso.getAlunos().contains(session.getUsuarioLogado())) {
            curso.getAlunos().remove(session.getUsuarioLogado());
            curso.getAlunos().remove(session.getUsuarioLogado());
        }
        curso = cursoService.salvar(curso);
        session.setInstituicoesParticipadas(null);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public String deixarInstituicao() {
        if (instituicao.getAdministradores().contains(session.getUsuarioLogado())) {
            instituicao.getAdministradores().remove(session.getUsuarioLogado());
            instituicao.getAdministradores().remove(session.getUsuarioLogado());
        }
        instituicao = instituicaoService.salvar(instituicao);
        session.setInstituicoesParticipadas(null);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public String solicitarParticipacaoCurso() {
        notificacaoService.gerarNotificacaoDeSoliciticaoDeIngressoParaCurso(session.getUsuarioLogado(), curso);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public boolean possuiSolicitacaoEntradaPendente() {
        return cursoService.possuiSolicitacaoDeIngressoPendente(session.getUsuarioLogado(), curso);
    }

    public String removerDaInstituicao(Usuario usuario) {
        notificacaoService.gerarNotificacaoDeRemocaoDeAdministradorDeInstituicao(usuario, instituicao);
        if (instituicao.getAdministradores().contains(usuario)) {
            instituicao.getAdministradores().remove(usuario);
        }
        instituicao = instituicaoService.salvar(instituicao);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }
    
    public String removerProfessorDoCurso(Usuario professor) {
        notificacaoService.gerarNotificacaoDeRemocaoDeProfesorDeCurso(professor, curso);
        if (curso.getProfessores().contains(professor)) {
            curso.getProfessores().remove(professor);
            curso.getProfessores().remove(professor);
        }
        notificacaoService.removerCursoMembroAva(professor, curso);
        curso = cursoService.salvar(curso);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }
    
    public String removerAlunoDoCurso(Usuario aluno) {
        notificacaoService.gerarNotificacaoDeRemocaoDeAlunoDeCurso(aluno, curso);
        if (curso.getAlunos().contains(aluno)) {
            curso.getAlunos().remove(aluno);
            curso.getAlunos().remove(aluno);
        }
        notificacaoService.removerCursoMembroAva(aluno, curso);
        curso = cursoService.salvar(curso);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public String cancelarAlteracoes() {
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public String salvar() {
        if (instituicao.isNew()) {
            instituicao.setAdministradores(new ArrayList<Usuario>());
            instituicao.getAdministradores().add(session.getUsuarioLogado());
        }
        session.setInstituicoesParticipadas(null);
        instituicao = instituicaoService.salvar(instituicao);
        return "instituicao.xhtml" + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public InstituicaoTipoAva[] getTiposAva() {
        return InstituicaoTipoAva.values();
    }

    public String getNovoCurso() {
        return novoCurso;
    }

    public void setNovoCurso(String novoCurso) {
        this.novoCurso = novoCurso;
    }

    public Curso getCurso() {
        if (curso == null) {
            curso = new Curso();
        }
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public String adicionarCurso() {
        Curso curso = new Curso();
        curso.setNome(getNovoCurso());
        curso.setInstituicao(instituicao);
        cursoService.salvar(curso);
        setNovoCurso(null);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public String excluirCurso(Curso curso) {
        instituicao.getCursos().remove(curso);
        instituicao = instituicaoService.salvar(instituicao);
        cursoService.removerCurso(curso);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public void editarCurso(Curso cursoEditar) {
        this.curso = cursoEditar;
    }

    public void salvarCurso() {
        curso = cursoService.salvar(curso);
    }

    public void load() {
        this.instituicao = instituicaoService.buscarPorId(instituicao.getId());
        instituicaoSeguida = isInstituicaoAtualSeguida();
    }

    public String editarInstituicao() {
        return "incluirEditarInstituicao.xhtml" + FacesNavigationUtil.FACES_REDIRECT + "&instituicaoId=" + instituicao.getId();
    }

    public boolean isInstituicaoSeguida() {
        return this.instituicaoSeguida;
    }

    public void setInstituicaoSeguida(boolean instituicaoSeguida) {
        this.instituicaoSeguida = instituicaoSeguida;
    }

    public boolean isParteInstituicao() {
        return instituicao.getProfessores().contains(session.getUsuarioLogado())
                || instituicao.getAlunos().contains(session.getUsuarioLogado())
                || instituicao.getAdministradores().contains(session.getUsuarioLogado());
    }

    public boolean isInstituicaoAtualSeguida() {
        InstituicaoSeguidaPK instituicaoSeguidaPK = new InstituicaoSeguidaPK(session.getUsuarioLogado(), instituicao);
        InstituicaoSeguida intituicaoSeguida = new InstituicaoSeguida();
        intituicaoSeguida.setId(instituicaoSeguidaPK);
        for (InstituicaoSeguida instituicaoSession : session.getInstituicoesSeguidas()) {
            if (instituicaoSession.getInstituicao().equals(instituicao)) {
                return true;
            }
        }
        return false;
    }
    
    public String convidarProfessorParaAva(Usuario membro) {
        notificacaoService.gerarNotificacaoDeConviteParaAva(membro, curso, CursoMembroAvaTipo.PROFESSOR);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }
    
    public String convidarAlunoParaAva(Usuario membro) {
        notificacaoService.gerarNotificacaoDeConviteParaAva(membro, curso, CursoMembroAvaTipo.ALUNO);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }
    
    public boolean conviteExistente(Usuario membro) {
        return notificacaoService.buscarCursoMembro(membro, curso) != null;
    }

    public void seguirDeixarDeSeguir() {
        if (!isInstituicaoSeguida()) {
            instituicaoService.desmarcarInstituicaoSeguida(session.getUsuarioLogado(), instituicao);
        } else {
            instituicaoService.marcarInstituicaoSeguida(session.getUsuarioLogado(), instituicao);
        }
        session.setInstituicoesSeguidas(instituicaoService.buscarInstituicoesSeguidas(session.getUsuarioLogado()));
    }

}

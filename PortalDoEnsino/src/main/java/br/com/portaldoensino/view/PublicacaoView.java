package br.com.portaldoensino.view;

import br.com.portaldoensino.entity.Curso;
import br.com.portaldoensino.entity.Grupo;
import br.com.portaldoensino.entity.Instituicao;
import br.com.portaldoensino.entity.Publicacao;
import br.com.portaldoensino.entity.PublicacaoTipo;
import br.com.portaldoensino.entity.Tema;
import br.com.portaldoensino.entity.Usuario;
import br.com.portaldoensino.service.CursoService;
import br.com.portaldoensino.service.GrupoService;
import br.com.portaldoensino.service.InstituicaoService;
import br.com.portaldoensino.service.PublicacaoService;
import br.com.portaldoensino.service.TemaService;
import br.com.portaldoensino.sessao.SessionController;
import br.com.portaldoensino.view.util.FacesNavigationUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class PublicacaoView implements Serializable {

    @Inject
    private SessionController session;
    @Inject
    private PublicacaoService publicacaoService;
    @Inject
    private TemaService temaService;
    @Inject
    private GrupoService grupoService;
    @Inject
    private InstituicaoService instituicaoService;
    @Inject
    private CursoService cursoService;
    private Publicacao publicacao;
    private Publicacao comentario;
    private List<Publicacao> publicacoes;
    private Grupo grupo;
    private Instituicao instituicao;
    private Curso curso;

    public Publicacao getPublicacao() {
        if (publicacao == null) {
            publicacao = new Publicacao();
        }
        return publicacao;
    }

    public void setPublicacao(Publicacao publicacao) {
        this.publicacao = publicacao;
    }

    public Publicacao getComentario() {
        if (comentario == null) {
            comentario = new Publicacao();
        }
        return comentario;
    }

    public Curso getCurso() {
        if (curso == null) {
            curso = new Curso();
        }
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public void setComentario(Publicacao comentario) {
        this.comentario = comentario;
    }

    public void salvarTema(Tema tema) {
        if (tema.isNew()) {
            Character.toUpperCase(tema.getNome().charAt(0));
            temaService.salvar(tema);
        }
    }

    public void loadPublicacao() {
        publicacao = publicacaoService.buscarPorId(publicacao.getId());
        if (publicacao != null && publicacao.getPublicacaoPai() == null) {
            List<Publicacao> publicacoesList = new ArrayList<>();
            publicacoesList.add(publicacao);
            publicacoes = publicacoesList;
        } else {
            publicacoes = new ArrayList<>();
        }
    }

    public void loadPublicacoesPorUsuario(Long id) {
        Usuario usuario = new Usuario();
        usuario.setId(id);
        publicacoes = publicacaoService.buscarPublicacoesPorUsuarioFetchTemas(usuario);
    }

    public String salvar() {
        for (Tema tema : publicacao.getTemas()) {
            salvarTema(tema);
        }
        if (!getGrupo().isNew()) {
            publicacao.setGrupo(getGrupo());
        }
        if (!getInstituicao().isNew()) {
            publicacao.setInstituicao(instituicao);
        }
        if (!getCurso().isNew()) {
            publicacao.setCurso(curso);
        }
        if (publicacao.isNew()) {
            publicacao.setUsuarioCadastro(session.getUsuarioLogado());
        }
        publicacao.setModoEdicao(false);
        publicacaoService.salvar(publicacao);
        if (!getGrupo().isNew()
                || !getInstituicao().isNew()
                || !getCurso().isNew()) {
            return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
        }
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT;
    }

    public String excluir(Publicacao publicacaoExclusao) {
        publicacaoService.excluir(publicacaoExclusao);
        publicacoes = null;
        if (!getGrupo().isNew()) {
            return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
        }
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT;
    }

    public boolean comentarioModoEdicao(Publicacao comentario) {
        return comentario.isModoEdicao();
    }

    public void editarComentario(Publicacao comentario) {
        comentario.setModoEdicao(true);
    }

    public void cancelarAlteracaoComentario(Publicacao comentario) {
        comentario.setModoEdicao(false);
    }

    public String excluirComentario(Publicacao publicacao, Publicacao comentario) {
        loadComentarios(publicacao);
        publicacao.getComentarios().remove(comentario);
        publicacao.setVisualizandoComentarios(true);
        publicacaoService.excluir(comentario);
        if (!getGrupo().isNew()) {
            return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
        }
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT;
    }

    public void atualizarComentario(Publicacao publicacao, Publicacao comentario) {
        setComentario(publicacaoService.buscarPorIdFetchComentarios(publicacaoService.salvar(comentario).getId()));
        loadComentarios(publicacao);
        publicacao.getComentarios().set(publicacao.getComentarios().indexOf(comentario), comentario);
        comentario.setModoEdicao(false);
        setComentario(null);
    }

    public void salvarComentario(Publicacao publicacao) {
        getComentario().setPublicacaoPai(publicacao);
        if (getComentario().isNew()) {  
            getComentario().setUsuarioCadastro(session.getUsuarioLogado());
        }
        setComentario(publicacaoService.buscarPorIdFetchComentarios(publicacaoService.salvar(comentario).getId()));
        loadComentarios(publicacao);
        publicacao.setVisualizandoComentarios(true);
        setComentario(null);
    }

    public void editarPublicacao(Publicacao publicacaoEdicao) {
        publicacaoEdicao.setModoEdicao(true);
        publicacao = publicacaoEdicao;
    }

    public void cancelarPublicacao() {
        publicacao = new Publicacao();
    }

    public void comentarPublicacao(Publicacao publicacao) {
        getComentario().setPublicacaoPai(publicacao);
    }

    public void visualizarComentarios(Publicacao publicacao) {
        loadComentarios(publicacao);
        if (publicacao.isVisualizandoComentarios()) {
            publicacao.setVisualizandoComentarios(false);
        } else {
            publicacao.setVisualizandoComentarios(true);
        }
    }

    public void aprovarDesaprovarPublicacao(Publicacao publicacao) {
        if (publicacao.isAprovada(session.getUsuarioLogado())) {
            publicacaoService.desaprovarPublicacao(session.getUsuarioLogado(), publicacao);
        } else {
            publicacaoService.aprovarPublicacao(session.getUsuarioLogado(), publicacao);
        }
    }

    public boolean publicacaoAprovada(Publicacao publicacao) {
        return publicacao.isAprovada(session.getUsuarioLogado());
    }

    public void loadComentarios(Publicacao publicacao) {
        Publicacao publicacaoBanco = publicacaoService.buscarPorIdFetchComentarios(publicacao.getId());
        publicacao.setComentarios(publicacaoBanco.getComentarios());
    }

    public List<Tema> todosTemas(String query) {
        List<Tema> todosTemas = temaService.buscarTodos();
        List<Tema> temasDisponiveis = new ArrayList<>();
        for (Tema tema : todosTemas) {
            tema.setNome(tema.getNome().toLowerCase());
            query = query.toLowerCase();
            if (tema.getNome().contains(query)) {
                temasDisponiveis.add(tema);
            }
        }
        Tema novoTema = new Tema();
        novoTema.setNome(query);
        if (temaService.buscarPorNome(query) == null) {
            temasDisponiveis.add(novoTema);
        }
        return temasDisponiveis;
    }

    public List<Publicacao> getPublicacoes() {
        if (publicacoes == null) {
            publicacoes = publicacaoService.buscarPublicacoesIndex(session.getUsuarioLogado(),
                    grupoService.buscarGruposParticipados(session.getUsuarioLogado()));
        }
        return publicacoes;
    }

    public void loadTema(Long temaId) {
        List<Tema> temas = new ArrayList<>();
        Tema tema1 = new Tema();
        tema1.setId(temaId);
        temas.add(tema1);
        publicacoes = publicacaoService.buscarPublicacoesPorTemasFetchTemas(temas);
    }

    public void loadGrupo(Long grupoId) {
        grupo = grupoService.buscarPorId(grupoId);
        publicacoes = publicacaoService.buscarPublicacoesPorGrupo(grupo);
    }
    
    public void loadInstituicao(Long instituicaoId) {
        instituicao = instituicaoService.buscarPorId(instituicaoId);
        publicacoes = publicacaoService.buscarPublicacoesPorInstituicao(instituicao);
    }
    
    public void loadCurso(Long cursoId) {
        curso = cursoService.buscarPorId(cursoId);
        publicacoes = publicacaoService.buscarPublicacoesPorCurso(curso);
    }

    public Grupo getGrupo() {
        if (grupo == null) {
            grupo = new Grupo();
        }
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public Instituicao getInstituicao() {
        if (instituicao == null) {
            instituicao = new Instituicao();
        }
        return instituicao; 
    }

    public void setInstituicao(Instituicao instituicao) {
        this.instituicao = instituicao;
    }
    
    public PublicacaoTipo[] getTiposPublicacao() {
        return PublicacaoTipo.values();
    }

    public PublicacaoTipo getTipoPublicacaoConhecimento() {
        return PublicacaoTipo.CONHECIMENTO;
    }

    public String visualizarTema() {
        return "/tema/tema.xhtml" + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public String visualizarPublicacao(Publicacao publicacao) {
        return "/publicacao/publicacao.xhtml" + FacesNavigationUtil.FACES_REDIRECT + "&publicacaoId=" + publicacao.getId();
    }

    public void marcarDesmarcarComentarioComoMelhorResposta(Publicacao comentario) {
        Publicacao publicacaoPai = comentario.getPublicacaoPai();
        if (publicacaoPai.getRespostaCorreta() != null
                && publicacaoPai.getRespostaCorreta().equals(comentario)) {
            publicacaoPai.setRespostaCorreta(null);
            publicacao = publicacaoService.salvar(publicacaoPai);
            publicacao.setVisualizandoComentarios(true);
            int index = 0;
            for (Publicacao publicacaoIt : publicacoes) {
                if (publicacaoIt.getId().equals(publicacao.getId())) {
                    break;
                }
                index++;
            }
            publicacoes.set(index, publicacao);
        } else {
            publicacaoPai.setRespostaCorreta(comentario);
            publicacao = publicacaoService.salvar(publicacaoPai);
            publicacao.setVisualizandoComentarios(true);
            int index = 0;
            for (Publicacao publicacaoIt : publicacoes) {
                if (publicacaoIt.getId().equals(publicacao.getId())) {
                    break;
                }
                index++;
            }
            publicacoes.set(index, publicacao);
        }
        publicacao = null;
    }
    
    public void habilitarComentario(Publicacao comentario) {
        comentario.setModoEdicao(true);
    }

}

package br.com.portaldoensino.view;

import br.com.portaldoensino.entity.Notificacao;
import br.com.portaldoensino.entity.NotificacaoTipo;
import br.com.portaldoensino.service.NotificacaoService;
import br.com.portaldoensino.service.UsuarioService;
import br.com.portaldoensino.sessao.SessionController;
import br.com.portaldoensino.view.util.FacesNavigationUtil;
import java.io.Serializable;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class NotificacaoView implements Serializable {

    @Inject
    private SessionController session;
    @Inject
    private UsuarioService usuarioService;
    @Inject
    private NotificacaoService notificacaoService;
    private List<Notificacao> notificacoes;

    public List<Notificacao> getNotificacoes() {
        if (notificacoes == null) {
            notificacoes = notificacaoService.buscarTodosPorUsuario(session.getUsuarioLogado());
        }
        return notificacoes;
    }

    public void setNotificacoes(List<Notificacao> notificacoes) {
        this.notificacoes = notificacoes;
    }

    public String confirmarNotificacao(Notificacao notificacao) {
        notificacao.setFinalizado(Boolean.TRUE);
        notificacao = notificacaoService.salvar(notificacao);
        session.setUsuarioLogado(usuarioService.buscarPorId(notificacao.getUsuarioNotificado().getId()));
        session.setGruposParticipados(null);
        session.setInstituicoesParticipadas(null);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT;
    }

    public String aceitarNotificacao(Notificacao notificacao) {
        notificacao.setFinalizado(Boolean.TRUE);
        notificacao.setNotificacaoAceita(Boolean.TRUE);
        notificacao = notificacaoService.salvar(notificacao);
        if (notificacao.getNotificacaoTipo().equals(NotificacaoTipo.AMIZADE_REMOVIDA)
                || notificacao.getNotificacaoTipo().equals(NotificacaoTipo.PEDIDO_AMIZADE_ACEITO)
                || notificacao.getNotificacaoTipo().equals(NotificacaoTipo.PEDIDO_AMIZADE)) {
            session.setUsuarioLogado(notificacao.getUsuarioNotificado());
        } else if (notificacao.getNotificacaoTipo().equals(NotificacaoTipo.GRUPO_INGRESSO_ACEITO)
                || notificacao.getNotificacaoTipo().equals(NotificacaoTipo.GRUPO_CONVITE_MEMBRO)) {
            session.setGruposParticipados(null);
        } else if (notificacao.getNotificacaoTipo().equals(NotificacaoTipo.INSTITUICAO_REMOCAO_ADMINISTRADOR)
                || notificacao.getNotificacaoTipo().equals(NotificacaoTipo.INSTITUICAO_CONVITE_ADMINITRADOR)
                || notificacao.getNotificacaoTipo().equals(NotificacaoTipo.CURSO_CONVITE_ALUNO)
                || notificacao.getNotificacaoTipo().equals(NotificacaoTipo.CURSO_CONVITE_PROFESSOR)
                || notificacao.getNotificacaoTipo().equals(NotificacaoTipo.CURSO_INGRESSO_ACEITO)
                || notificacao.getNotificacaoTipo().equals(NotificacaoTipo.CURSO_REMOCAO_ALUNO)
                || notificacao.getNotificacaoTipo().equals(NotificacaoTipo.CURSO_REMOCAO_PROFESSOR)) {
            session.setInstituicoesParticipadas(null);
        } else if (notificacao.getNotificacaoTipo().equals(NotificacaoTipo.AVA_CONVITE)) {
            session.setAvasDisponiveis(null);
        }
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT;
    }

    public String recusarNotificacao(Notificacao notificacao) {
        notificacao.setFinalizado(Boolean.TRUE);
        notificacao.setNotificacaoAceita(Boolean.FALSE);
        notificacaoService.salvar(notificacao);
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT;
    }

}

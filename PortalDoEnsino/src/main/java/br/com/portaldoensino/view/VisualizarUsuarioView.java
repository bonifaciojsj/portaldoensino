package br.com.portaldoensino.view;

import br.com.portaldoensino.entity.Grupo;
import br.com.portaldoensino.entity.Instituicao;
import br.com.portaldoensino.entity.Usuario;
import br.com.portaldoensino.entity.UsuarioAmigo;
import br.com.portaldoensino.service.GrupoService;
import br.com.portaldoensino.service.InstituicaoService;
import br.com.portaldoensino.service.NotificacaoService;
import br.com.portaldoensino.service.UsuarioService;
import br.com.portaldoensino.sessao.SessionController;
import br.com.portaldoensino.view.util.FacesNavigationUtil;
import java.io.Serializable;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class VisualizarUsuarioView implements Serializable {

    @Inject
    private SessionController session;
    @Inject
    private UsuarioService usuarioService;
    @Inject
    private NotificacaoService notificacaoService;
    @Inject
    private GrupoService grupoService;
    @Inject
    private InstituicaoService instituicaoService;
    private Usuario usuario;

    public Usuario getUsuario() {
        if (usuario == null) {
            usuario = new Usuario();
        }
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public void load() {
        usuario = usuarioService.buscarPorId(usuario.getId());
    }

    public boolean pedidoAmizadePendente() {
        return notificacaoService.possuiPedidoAmizadePendente(session.getUsuarioLogado(), getUsuario());
    }

    public String adicionarAmigo() {
        notificacaoService.gerarNotificacaoDeSolicitacaoDeAmizade(session.getUsuarioLogado(), getUsuario());
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }

    public String desfazerAmizade(Usuario usuarioLogado, Usuario usuarioAmigo) {
        UsuarioAmigo amizade1 = usuarioService.buscarPorUsuarios(usuarioLogado, usuarioAmigo);
        usuarioService.excluirAmizade(amizade1);
        notificacaoService.gerarNotificacaoDeAmizadeRemovida(session.getUsuarioLogado(), getUsuario());
        session.setUsuarioLogado(usuarioService.buscarPorId(usuarioLogado.getId()));
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }
    
    public String seguirUsuario() {
        usuarioService.seguirUsuario(session.getUsuarioLogado(), usuario);
        session.setUsuarioLogado(usuarioService.buscarPorId(session.getUsuarioLogado().getId()));
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }
    
    public String deixarDeSeguirUsuario() {
        usuarioService.deixarDeSeguirUsuario(session.getUsuarioLogado(), usuario);
        session.setUsuarioLogado(usuarioService.buscarPorId(session.getUsuarioLogado().getId()));
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
    }
    
    public List<Grupo> getGruposParticipados() {
        return grupoService.buscarGruposParticipados(usuario);
    }
    
    public List<Instituicao> getInstituicoesParticipadas() {
        return instituicaoService.buscarInstituicoesParticipadas(usuario);
    }
    
    public String visualizarGrupo(Grupo grupo) {
        return "/grupo/grupo.xhtml" + FacesNavigationUtil.FACES_REDIRECT + "&grupoId=" + grupo.getId();
    }
    
    public String visualizarInstituicao(Instituicao instituicao) {
        return "/instituicao/instituicao.xhtml" + FacesNavigationUtil.FACES_REDIRECT + "&instituicaoId=" + instituicao.getId();
    }
    
    public String novaMensagem() {
        return "/secured/mensagem/mensagem.xhtml" + FacesNavigationUtil.FACES_REDIRECT + "&usuarioId=" + usuario.getId();
    }
}

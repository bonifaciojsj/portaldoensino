package br.com.portaldoensino.view;

import br.com.portaldoensino.entity.Usuario;
import br.com.portaldoensino.exception.PortalDoEnsinoException;
import br.com.portaldoensino.service.UsuarioService;
import br.com.portaldoensino.util.SenhaUtil;
import br.com.portaldoensino.view.util.FacesMessageUtil;
import br.com.portaldoensino.view.util.FacesNavigationUtil;
import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class CadastroView implements Serializable {
    
    @Inject
    private UsuarioService usuarioService;
    private Usuario usuario;
    private String senhaConfirmacao;

    public Usuario getUsuario() {
        if (usuario == null) {
            usuario = new Usuario();
        }
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getSenhaConfirmacao() {
        return senhaConfirmacao;
    }

    public void setSenhaConfirmacao(String senhaConfirmacao) {
        this.senhaConfirmacao = SenhaUtil.getEncryptedPassword(senhaConfirmacao);
    }
    
    public String salvar() {
        try {
            usuarioService.salvarNovoUsuario(getUsuario(), getSenhaConfirmacao());
            FacesMessageUtil.addInfoMessage("Usuário salvo com sucesso, você pode logar-se com este novo usuário agora.", true);
            return FacesNavigationUtil.REDIRECT_WELCOME_PAGE;
        } catch (PortalDoEnsinoException ex) {
            FacesMessageUtil.addErrorMessage(ex.getLocalizedMessage(), false);
            return null;
        }
    }
    
}

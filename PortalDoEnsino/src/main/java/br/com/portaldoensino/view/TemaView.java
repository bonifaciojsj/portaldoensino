package br.com.portaldoensino.view;

import br.com.portaldoensino.entity.Tema;
import br.com.portaldoensino.entity.TemaFavorito;
import br.com.portaldoensino.entity.TemaFavoritoPK;
import br.com.portaldoensino.service.TemaService;
import br.com.portaldoensino.sessao.SessionController;
import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author BONIFACIO
 */
@Named
@ViewScoped
public class TemaView implements Serializable {

    @Inject
    private SessionController sessionController;
    @Inject
    private TemaService temaService;
    private Tema tema;
    private boolean favorito;

    public Tema getTema() {
        if (tema == null) {
            tema = new Tema();
        }
        return tema;
    }

    public void setTema(Tema tema) {
        this.tema = tema;
    }
    
    public void load() {
        tema = temaService.buscarPorId(tema.getId());
        favorito = isTemaAtualFavorito();
    }

    public boolean isFavorito() {   
        return favorito;
    }

    public void setFavorito(boolean favorito) {
        this.favorito = favorito;
    }
    
    public boolean isTemaAtualFavorito() {        
        TemaFavoritoPK favoritoPK = new TemaFavoritoPK(sessionController.getUsuarioLogado(), tema);
        TemaFavorito temaFavorito = new TemaFavorito();
        temaFavorito.setId(favoritoPK); 
        for (TemaFavorito temaSession : sessionController.getTemasFavoritos()) {
            if (temaSession.getTema().equals(tema)) {
                return true;
            }
        }
        return false;
    }
    
    public void marcarDesmarcarFavorito() {
        if (!isFavorito()) {
            temaService.desmarcarTemaComoFavorito(sessionController.getUsuarioLogado(), tema);
        } else {           
            temaService.marcarTemaComoFavorito(sessionController.getUsuarioLogado(), tema);
        }
        sessionController.setTemasFavoritos(temaService.buscarTemasFavoritos(sessionController.getUsuarioLogado()));
    }
    
}

package br.com.portaldoensino.view.util;

/**
 *
 * @author jose.bonifacio
 */
public class FacesNavigationUtil {

    public static final String OUTCOME_LISTAR = "listar.xhtml";
    public static final String OUTCOME_INCLUIR_EDITAR = "incluirEditar.xhtml";
    public static final String OUTCOME_REPORT = "report.xhtml";
    public static final String FACES_REDIRECT = "?faces-redirect=true";
    public static final String FACES_REDIRECT_SEND_PARAMETERS = FACES_REDIRECT + "&includeViewParams=true";
    public static final String REDIRECT_WELCOME_PAGE = "/index.xhtml" + FACES_REDIRECT;
    public static final String REDIRECT_OUTCOME_LISTAR = OUTCOME_LISTAR + FACES_REDIRECT;
    public static final String REDIRECT_OUTCOME_INCLUIR_EDITAR = OUTCOME_INCLUIR_EDITAR + FACES_REDIRECT;
    public static final String REDIRECT_OUTCOME_REPORT = OUTCOME_REPORT + FACES_REDIRECT;
        
}

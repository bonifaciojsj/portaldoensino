package br.com.portaldoensino.converter;

import br.com.portaldoensino.entity.Tema;
import br.com.portaldoensino.service.TemaService;
import java.io.Serializable;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
public class TemaConverter implements Converter, Serializable {

    @Inject 
    private TemaService temaService;

    @Override
    public Tema getAsObject(FacesContext fc, UIComponent component, String string) {
        if (string == null || string.isEmpty()) {
            return null;
        }
        Tema tema = temaService.buscarPorNome(string);
        if (tema == null) {
            tema = new Tema();
            tema.setNome(string);
        }
        return tema;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent component, Object tema) {
            return String.valueOf(((Tema) tema).getNome());

    }
}

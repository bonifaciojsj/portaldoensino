package br.com.portaldoensino.converter;

import br.com.portaldoensino.util.DateUtil;
import java.sql.Date;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;

/**
 *
 * @author jose.bonifacio
 */
public class DateConverter implements javax.faces.convert.Converter {  
  
    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2)  
            throws ConverterException {  
        return arg2;  
    }  
  
    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2)  
            throws ConverterException {  
        if(arg2 == null){  
            return "";  
        }  
        return DateUtil.getFormattedDate((Date)arg2);  
    }  
}

package br.com.portaldoensino.converter;

import java.io.Serializable;
import java.lang.reflect.Field;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;

/**
 *
 * @author jose.bonifacio
 */
@Named
public class GenericEntityConverter implements Converter, Serializable {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Object getAsObject(FacesContext fc, UIComponent component, String string) {
        try {
            String[] split = string.split(":");
            return em.find(Class.forName(split[0]), Long.valueOf(split[1]));
        } catch (NumberFormatException | ClassNotFoundException e) {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent component, Object object) {
        try {
            Class<? extends Object> clazz = object.getClass();
            for (Field f : clazz.getDeclaredFields()) {
                f.setAccessible(true);
                if (f.isAnnotationPresent(Id.class)) {
                    f.setAccessible(true);
                    Long id = (Long) f.get(object);
                    if (id != null) {
                        return clazz.getCanonicalName() + ":" + id.toString();                        
                    }
                    return "";
                }
            }
        } catch (IllegalArgumentException | IllegalAccessException e) {
        }
        return null;
    }

}

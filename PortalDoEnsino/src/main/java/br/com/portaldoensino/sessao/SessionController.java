package br.com.portaldoensino.sessao;

import br.com.portaldoensino.entity.*;
import br.com.portaldoensino.service.*;
import br.com.portaldoensino.util.MoodleUtil;
import br.com.portaldoensino.util.SenhaUtil;
import br.com.portaldoensino.view.util.FacesMessageUtil;
import br.com.portaldoensino.view.util.FacesNavigationUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.menu.*;

/**
 * Simple login bean.
 *
 * @author itcuties
 */
@Named
@SessionScoped
public class SessionController implements Serializable {

    @Inject
    private UsuarioService usuarioService;
    @Inject
    private NotificacaoService notificacaoService;
    @Inject
    private MensagemService mensagemService;
    @Inject
    private MensagemChatService mensagemChatService;
    @Inject
    private TemaService temaService;
    @Inject
    private GrupoService grupoService;
    @Inject
    private InstituicaoService instituicaoService;

    private String username;
    private String password;

    private boolean loggedIn;
    private Usuario usuarioLogado;
    private List<Notificacao> notificacoes;
    private String mensagemChat;
    private List<MensagemChat> mensagensChat;
    private List<Mensagem> mensagens;
    private List<Mensagem> mensagensNaoLidas;
    private UsuarioAmigo usuarioConversa;
    private List<UsuarioAmigo> amigosUsuarioLogado;
    private List<TemaFavorito> temasFavoritos;
    private List<Grupo> gruposParticipados;
    private List<Instituicao> instituicoesParticipadas;
    private List<InstituicaoSeguida> instituicoesSeguidas;
    private List<CursoMembroAva> avasDisponiveis;
    private String stringConsulta;

    public String doLogin() {
        Usuario usuario = usuarioService.buscarAtivosPorUsuarioESenha(username, password);
        if (usuario != null) {
            loggedIn = true;
            setUsuarioLogado(usuario);
            return FacesContext.getCurrentInstance().getViewRoot().getViewId() + FacesNavigationUtil.FACES_REDIRECT_SEND_PARAMETERS;
        } else {
            FacesMessageUtil.addErrorMessage("Usuário/Senha inválidos.", true);
            return null;
        }
    }

    /**
     * Logout operation.
     *
     * @return
     */
    public String doLogout() {
        loggedIn = false;
        setUsuarioLogado(null);
        setUsuarioConversa(null);
        setMensagens(null);
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return FacesNavigationUtil.REDIRECT_WELCOME_PAGE;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public Usuario getUsuarioLogado() {
        if (usuarioLogado == null) {
            usuarioLogado = new Usuario();
        }
        return usuarioLogado;
    }

    public void setUsuarioLogado(Usuario usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }

    public List<UsuarioAmigo> getAmigosUsuarioLogado() {
        return usuarioService.buscarAmigosPorUsuario(usuarioLogado);
    }

    public void setAmigosUsuarioLogado(List<UsuarioAmigo> amigosUsuarioLogado) {
        this.amigosUsuarioLogado = amigosUsuarioLogado;
    }

    public UsuarioAmigo getUsuarioConversa() {
        return usuarioConversa;
    }

    public void setUsuarioConversa(UsuarioAmigo usuarioConversa) {
        this.usuarioConversa = usuarioConversa;
    }

    public String getMensagensChatNaoLidas() {
        int quantidadeMensagensNaoLidas = 0;
        for (MensagemChat mensagemChat : getMensagensChat()) {
            if (usuarioConversa.getDataUltimaVisualizacao().before(mensagemChat.getDataEnvio())) {
                quantidadeMensagensNaoLidas++;
            }
        }
        return String.valueOf(quantidadeMensagensNaoLidas);
    }

    public List<MensagemChat> getMensagensChat() {
        if (mensagensChat == null) {
            mensagensChat = new ArrayList<>();
            if (usuarioLogado != null && usuarioConversa != null) {
                mensagensChat.addAll(mensagemChatService.buscarMensagensEntreUsuarios(usuarioLogado, usuarioConversa.getAmigo()));
            }
        }
        return mensagensChat;
    }

    public void setMensagens(List<MensagemChat> mensagensChat) {
        this.mensagensChat = mensagensChat;
    }

    public List<TemaFavorito> getTemasFavoritos() {
        if (temasFavoritos == null) {
            temasFavoritos = temaService.buscarTemasFavoritos(usuarioLogado);
        }
        return temasFavoritos;
    }
    
    public void setGruposParticipados(List<Grupo> gruposParticipados) {
        this.gruposParticipados = gruposParticipados;
    }
    
    public List<Grupo> getGruposParticipados() {
        if (gruposParticipados == null) {
            gruposParticipados = grupoService.buscarGruposParticipados(usuarioLogado);
        }
        return gruposParticipados;
    }
    
    public void setInstituicoesParticipadas(List<Instituicao> instituicoesParticipadas) {
        this.instituicoesParticipadas = instituicoesParticipadas;
    }
    
    public List<Instituicao> getInstituicoesParticipadas() {
        if (instituicoesParticipadas == null) {
            instituicoesParticipadas = instituicaoService.buscarInstituicoesParticipadas(usuarioLogado);
        }
        return instituicoesParticipadas;
    }
    
    public List<CursoMembroAva> getAvasDisponiveis() {
        if (avasDisponiveis == null) {
            avasDisponiveis = notificacaoService.buscarAvasDisponiveis(usuarioLogado);
        }
        return avasDisponiveis;
    }
    
    public void setAvasDisponiveis(List<CursoMembroAva> avas) {
        this.avasDisponiveis = avas;
    }

    public List<InstituicaoSeguida> getInstituicoesSeguidas() {
        if (instituicoesSeguidas == null) {
            instituicoesSeguidas = instituicaoService.buscarInstituicoesSeguidas(usuarioLogado);
        }
        return instituicoesSeguidas;
    }

    public void setInstituicoesSeguidas(List<InstituicaoSeguida> instituicoesSeguidas) {
        this.instituicoesSeguidas = instituicoesSeguidas;
    }

    public void setTemasFavoritos(List<TemaFavorito> temasFavoritos) {
        this.temasFavoritos = temasFavoritos;
    }

    public String getStringConsulta() {
        return stringConsulta;
    }

    public void setStringConsulta(String stringConsulta) {
        this.stringConsulta = stringConsulta;
    }

    public MenuModel menuInicioBarraLateralEsquerda() {
        MenuModel menu = new DefaultMenuModel();
        DefaultMenuItem menuInicio = new DefaultMenuItem();
        menuInicio.setValue("Início");
        menuInicio.setIcon("homeIcon");
        menuInicio.setOutcome("/index.xhtml?faces-redirect=true");
        menuInicio.setStyleClass(FacesContext.getCurrentInstance().getViewRoot().getViewId().equals("/index.xhtml") ? "ui-state-active" : "");
        DefaultMenuItem menuMensagens = new DefaultMenuItem();
        menuMensagens.setValue(getContagemDeMensagensNaoLidas() + " - Mensagem(ns)");
        menuMensagens.setIcon("mensagensIcon");
        menuMensagens.setOutcome("/secured/mensagem/mensagens.xhtml?faces-redirect=true");
        menuMensagens.setStyleClass(FacesContext.getCurrentInstance().getViewRoot().getViewId().contains("/secured/mensagem/") ? "ui-state-active" : "");
        DefaultMenuItem menuNotificacoes = new DefaultMenuItem();
        menuNotificacoes.setValue(getContagemDeNotificacoesNaoFinalizadas() + " - Notificacoes");
        menuNotificacoes.setIcon("notificacaoIcon");
        menuNotificacoes.setOutcome("/secured/notificacao/notificacoes.xhtml?faces-redirect=true");
        menuNotificacoes.setStyleClass(FacesContext.getCurrentInstance().getViewRoot().getViewId().contains("/secured/notificacao/") ? "ui-state-active" : "");
        menu.addElement(menuInicio);
        menu.addElement(menuMensagens);
        menu.addElement(menuNotificacoes);
        return menu;
    }

    public MenuModel menuTemasBarraLateralEsquerda() {
        MenuModel menu = new DefaultMenuModel();
        DefaultSubMenu submenu = new DefaultSubMenu("Temas favoritos");
        for (TemaFavorito tema : getTemasFavoritos()) {
            DefaultMenuItem item = new DefaultMenuItem(tema.getTema().getNome());
            item.setOutcome("/tema/tema.xhtml?temaId=" + tema.getTema().getId() + "&faces-redirect=true");
            submenu.addElement(item);
        }
        menu.addElement(submenu);
        return menu;
    }

    public MenuModel menuGruposBarraLateralEsquerda() {
        MenuModel menu = new DefaultMenuModel();
        DefaultSubMenu submenu = new DefaultSubMenu("Grupos");
        for (Grupo grupo : getGruposParticipados()) {
            DefaultMenuItem item = new DefaultMenuItem(grupo.getNome());
            item.setOutcome("/grupo/grupo.xhtml?grupoId=" + grupo.getId() + "&faces-redirect=true");
            submenu.addElement(item);
        }
        DefaultMenuItem item = new DefaultMenuItem("(+) Novo grupo");
        item.setOutcome("/grupo/incluirEditarGrupo.xhtml");
        submenu.addElement(item);
        menu.addElement(submenu);
        return menu;
    }
    
    public MenuModel menuInstituicoesBarraLateralEsquerda() {
        MenuModel menu = new DefaultMenuModel();
        DefaultSubMenu submenu = new DefaultSubMenu("Instituições");
        for (Instituicao instituicao : getInstituicoesParticipadas()) {
            DefaultMenuItem item = new DefaultMenuItem(instituicao.getNome());
            item.setOutcome("/instituicao/instituicao.xhtml?instituicaoId=" + instituicao.getId() + "&faces-redirect=true");
            submenu.addElement(item);
        }
        DefaultMenuItem item = new DefaultMenuItem("(+) Nova instituição");
        item.setOutcome("/instituicao/incluirEditarInstituicao.xhtml");
        submenu.addElement(item);
        menu.addElement(submenu);
        return menu;
    }
    
    public MenuModel menuAvasBarraLateralEsquerda() {
        MenuModel menu = new DefaultMenuModel();
        DefaultSubMenu submenu = new DefaultSubMenu("AVAs disponíveis");
        for (CursoMembroAva ava : getAvasDisponiveis()) {
            DefaultMenuItem item = new DefaultMenuItem(ava.getCurso().getInstituicao().getNome());
            item.setUrl("localhost:8080/PortalDoEnsino/moodle?avaId=" + ava.getId());
            item.setTarget("_blank");
            submenu.addElement(item);
        }
        menu.addElement(submenu);
        return menu;
    }

    public String getMensagemChat() {
        return mensagemChat;
    }

    public void setMensagemChat(String mensagemChat) {
        this.mensagemChat = mensagemChat;
    }

    public List<Notificacao> getNotificacoes() {
        if (notificacoes == null) {
            notificacoes = notificacaoService.buscarTodosNaoFinalizadosPorUsuario(usuarioLogado);
        }
        return notificacoes;
    }

    public List<Mensagem> getMensagens() {
        if (mensagens == null) {
            mensagens = mensagemService.buscarTodosPorUsuario(usuarioLogado);
        }
        return mensagens;
    }

    public List<Mensagem> getMensagensNaoLidas() {
        if (mensagensNaoLidas == null) {
            mensagensNaoLidas = new ArrayList<>();
        }
        return mensagensNaoLidas;
    }

    public void setMensagensNaoLidas(List<Mensagem> mensagensNaoLidas) {
        this.mensagensNaoLidas = mensagensNaoLidas;
    }

    public String getContagemDeMensagensNaoLidas() {
        if (usuarioLogado != null) {
            mensagensNaoLidas = mensagemService.buscarTodosNaoLidosPorUsuario(usuarioLogado);
            return String.valueOf(mensagensNaoLidas.size());
        }
        return "0";
    }

    public String getContagemDeNotificacoesNaoFinalizadas() {
        if (usuarioLogado != null) {
            notificacoes = notificacaoService.buscarTodosNaoFinalizadosPorUsuario(usuarioLogado);
            return String.valueOf(getNotificacoes().size());
        }
        return "0";
    }

    public void atualizarNotificacoes() {
        notificacoes = notificacaoService.buscarTodosNaoFinalizadosPorUsuario(usuarioLogado);
    }

    public void atualizarMensagens() {
        if (usuarioLogado != null) {
            mensagens = mensagemService.buscarTodosPorUsuario(usuarioLogado);
        }
    }

    public void atualizarMensagensNaoLidas() {
        mensagensNaoLidas = mensagemService.buscarTodosNaoLidosPorUsuario(usuarioLogado);
    }

    public void atualizarChat() {
        usuarioLogado = usuarioService.buscarPorId(usuarioLogado.getId());
    }

    public void fecharChat() {
        usuarioConversa = null;
    }

    public void atualizarMensagemComAmigo() {
        mensagemChat = null;
        if (usuarioConversa != null) {
            usuarioConversa.setUsuario(usuarioLogado);
            usuarioService.salvarDataVisualizacaoUltimaMensagem(usuarioConversa);
            mensagensChat = mensagemChatService.buscarMensagensEntreUsuarios(usuarioLogado, usuarioConversa.getAmigo());
        }
    }

    public boolean ehProprioUsuario(Usuario usuario) {
        return usuarioLogado.equals(usuario);
    }
    
    public void enviarMensagemChat() {
        MensagemChat mensagemChat = new MensagemChat();
        mensagemChat.setUsuarioEnvio(usuarioLogado);
        mensagemChat.setUsuarioRecebimento(usuarioConversa.getAmigo());
        mensagemChat.setDataEnvio(new Date());
        mensagemChat.setMensagem(this.mensagemChat);
        this.mensagemChat = null;
        mensagemChatService.salvar(mensagemChat, usuarioService.buscarPorUsuarios(usuarioConversa.getAmigo(), usuarioLogado));
    }

    public String getAvatar() {
        return "images/defaultavatar2.png";
    }
    
    public String buscaPrincipal() {
        return "/consulta.xhtml" + FacesNavigationUtil.FACES_REDIRECT + "&consulta=" + getStringConsulta();
    }

}

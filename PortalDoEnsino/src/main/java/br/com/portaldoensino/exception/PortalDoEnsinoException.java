package br.com.portaldoensino.exception;

/**
 *
 * @author jose.bonifacio
 */
public class PortalDoEnsinoException extends Exception {

    public PortalDoEnsinoException(String message) {
        super(message);
    }

    public PortalDoEnsinoException(String message, Throwable cause) {
        super(message, cause);
    }
    
}

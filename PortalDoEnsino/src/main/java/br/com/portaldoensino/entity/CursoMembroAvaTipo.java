package br.com.portaldoensino.entity;

/**
 *
 * @author BONIFACIO
 */
public enum CursoMembroAvaTipo {
    
    PROFESSOR("Professor"),
    ALUNO("Aluno");
    
    private final String descricao;

    private CursoMembroAvaTipo(String descricao) {
        this.descricao = descricao;
    }
    
    public String getDescricao() {
        return this.descricao;
    }
}

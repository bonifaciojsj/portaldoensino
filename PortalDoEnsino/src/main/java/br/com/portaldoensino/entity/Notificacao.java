package br.com.portaldoensino.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jose.bonifacio
 */
@Entity
@Table(name = "notificacao")
public class Notificacao implements PersistentEntity<Long>, Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String descricao;
    @ManyToOne
    private Usuario usuarioNotificado;
    @ManyToOne
    private Usuario usuarioEnvio;
    @ManyToOne
    private Grupo grupo;
    @ManyToOne
    private Instituicao instituicao;
    @ManyToOne
    private Curso curso;
    @NotNull
    private Boolean finalizado;
    private Boolean notificacaoAceita;
    @Enumerated(EnumType.STRING)
    private NotificacaoTipo notificacaoTipo;
    
    public Notificacao() {
        finalizado = false;
        notificacaoAceita = null;
    }
    
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Usuario getUsuarioNotificado() {
        return usuarioNotificado;
    }

    public void setUsuarioNotificado(Usuario usuarioNotificado) {
        this.usuarioNotificado = usuarioNotificado;
    }

    public Boolean getFinalizado() {
        return finalizado;
    }

    public void setFinalizado(Boolean finalizado) {
        this.finalizado = finalizado;
    }

    public Boolean getNotificacaoAceita() {
        return notificacaoAceita;
    }

    public void setNotificacaoAceita(Boolean notificacaoAceita) {
        this.notificacaoAceita = notificacaoAceita;
    }

    public NotificacaoTipo getNotificacaoTipo() {
        return notificacaoTipo;
    }

    public void setNotificacaoTipo(NotificacaoTipo notificacaoTipo) {
        this.notificacaoTipo = notificacaoTipo;
    }    

    public Usuario getUsuarioEnvio() {
        return usuarioEnvio;
    }

    public void setUsuarioEnvio(Usuario usuarioEnvio) {
        this.usuarioEnvio = usuarioEnvio;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public Instituicao getInstituicao() {
        return instituicao;
    }

    public void setInstituicao(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }
    
    @Override
    public boolean isNew() {
        return this.id == null;
    }    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Notificacao other = (Notificacao) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

   
       
}

package br.com.portaldoensino.entity;

/**
 *
 * @author BONIFACIO
 */
public enum GrupoTipo {
    
    PRIVADO("Privado"),
    PUBLICO("Público");
    
    private final String descricao;

    private GrupoTipo(String descricao) {
        this.descricao = descricao;
    }
    
    public String getDescricao() {
        return this.descricao;
    }
}

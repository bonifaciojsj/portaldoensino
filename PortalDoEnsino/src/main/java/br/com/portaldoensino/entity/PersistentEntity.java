package br.com.portaldoensino.entity;

import java.io.Serializable;

/**
 *
 * @author jose.bonifacio
 * @param <Long>
 */
public interface PersistentEntity<Long extends Serializable> extends Serializable {
    
    public Long getId();

    public void setId(Long id);

    public boolean isNew(); 
    
}

package br.com.portaldoensino.entity;

/**
 *
 * @author BONIFACIO
 */
public enum NotificacaoTipo {  
    
    PEDIDO_AMIZADE(true),   
    PEDIDO_AMIZADE_ACEITO(false),
    AMIZADE_REMOVIDA(false),
    GRUPO_INGRESSO(true),
    GRUPO_INGRESSO_ACEITO(false),
    GRUPO_CONVITE_MEMBRO(true),
    GRUPO_CONVITE_ADMINISTRADOR(true),
    GRUPO_REMOCAO_ADMINISTRADOR(false),
    GRUPO_REMOCAO_MEMBRO(false),
    INSTITUICAO_CONVITE_ADMINITRADOR(true),
    INSTITUICAO_REMOCAO_ADMINISTRADOR(false),
    CURSO_CONVITE_PROFESSOR(true),
    CURSO_REMOCAO_PROFESSOR(false),
    CURSO_INGRESSO_ALUNO(true),
    CURSO_INGRESSO_ACEITO(false),
    CURSO_CONVITE_ALUNO(true),
    CURSO_REMOCAO_ALUNO(false),
    AVA_CONVITE(true),
    AVA_CONVITE_ACEITO(false);
    
    private final boolean necessitaAceitacao;

    private NotificacaoTipo(boolean necessitaAceitacao) {
        this.necessitaAceitacao = necessitaAceitacao;
    }
    
    public boolean isNecessitaAceitacao() {
        return necessitaAceitacao;
    }
    
}

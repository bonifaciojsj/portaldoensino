package br.com.portaldoensino.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author BONIFACIO
 */
@Entity
@Table(name = "instituicao_seguida")
public class InstituicaoSeguida implements Serializable, PersistentEntity<InstituicaoSeguidaPK> {

    @EmbeddedId
    private InstituicaoSeguidaPK id;
    @ManyToOne
    @JoinColumn(name = "usuario_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Usuario usuario;
    @ManyToOne
    @JoinColumn(name = "instituicao_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Instituicao instituicao;

    public InstituicaoSeguida() {
    }

    @Override
    public InstituicaoSeguidaPK getId() {
        return id;
    }

    @Override
    public void setId(InstituicaoSeguidaPK id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Instituicao getInstituicao() {
        return instituicao;
    }

    public void setInstituicao(Instituicao instituicao) {
        this.instituicao = instituicao;
    }
    
    @Override
    public boolean isNew() {
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InstituicaoSeguida other = (InstituicaoSeguida) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
}

package br.com.portaldoensino.entity;

import java.sql.Timestamp;

/**
 *
 * @author jose.bonifacio
 */
public class MensagemPorMensagemPai {

    private Mensagem mensagemPai;
    private int mensagensNaoLidas;
    private Timestamp ultimaMensagemEnviada;

    public MensagemPorMensagemPai(Mensagem mensagemPai, int mensagensNaoLidas, Timestamp ultimaMensagemEnviada) {
        this.mensagemPai = mensagemPai;
        this.mensagensNaoLidas = mensagensNaoLidas;
        this.ultimaMensagemEnviada = ultimaMensagemEnviada;
    }

    public Mensagem getMensagemPai() {
        return mensagemPai;
    }

    public void setMensagemPai(Mensagem usuario) {
        this.mensagemPai = mensagemPai;
    }

    public int getMensagensNaoLidas() {
        return mensagensNaoLidas;
    }

    public void setMensagensNaoLidas(int mensagensNaoLidas) {
        this.mensagensNaoLidas = mensagensNaoLidas;
    }

    public Timestamp getUltimaMensagemEnviada() {
        return ultimaMensagemEnviada;
    }

    public void setUltimaMensagemEnviada(Timestamp ultimaMensagemEnviada) {
        this.ultimaMensagemEnviada = ultimaMensagemEnviada;
    }
    
}

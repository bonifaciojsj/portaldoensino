package br.com.portaldoensino.entity;

/**
 *
 * @author BONIFACIO
 */
public enum InstituicaoTipoAva {
    
    MOODLE("Moodle");
    
    private final String descricao;

    private InstituicaoTipoAva(String descricao) {
        this.descricao = descricao;
    }
    
    public String getDescricao() {
        return this.descricao;
    }
}

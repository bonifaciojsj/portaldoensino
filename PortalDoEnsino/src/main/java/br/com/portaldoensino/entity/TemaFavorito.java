package br.com.portaldoensino.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author BONIFACIO
 */
@Entity
@Table(name = "tema_favorito")
public class TemaFavorito implements Serializable, PersistentEntity<TemaFavoritoPK> {

    @EmbeddedId
    private TemaFavoritoPK id;
    @ManyToOne
    @JoinColumn(name = "usuario_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Usuario usuario;
    @ManyToOne
    @JoinColumn(name = "tema_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Tema tema;

    public TemaFavorito() {
    }

    @Override
    public TemaFavoritoPK getId() {
        return id;
    }

    @Override
    public void setId(TemaFavoritoPK id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Tema getTema() {
        return tema;
    }

    public void setTema(Tema tema) {
        this.tema = tema;
    }
    
    @Override
    public boolean isNew() {
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TemaFavorito other = (TemaFavorito) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }       
    
}

package br.com.portaldoensino.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author jose.bonifacio
 */
@Entity
@Table(name = "publicacao_aprovar")
public class PublicacaoAprovar implements Serializable, PersistentEntity<PublicacaoAprovarPK>{
        
    @EmbeddedId
    private PublicacaoAprovarPK id;
    @ManyToOne
    @JoinColumn(name = "usuario_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Usuario usuario;    
    @ManyToOne
    @JoinColumn(name = "publicacao_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Publicacao publicacao;

    @Override
    public PublicacaoAprovarPK getId() {
        return id;
    }

    @Override
    public void setId(PublicacaoAprovarPK id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Publicacao getPublicacao() {
        return publicacao;
    }

    public void setPublicacao(Publicacao publicacao) {
        this.publicacao = publicacao;
    }
    
    @Override
    public boolean isNew() {
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PublicacaoAprovar other = (PublicacaoAprovar) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}

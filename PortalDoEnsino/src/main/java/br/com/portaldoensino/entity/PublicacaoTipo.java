package br.com.portaldoensino.entity;

/**
 *
 * @author jose.bonifacio
 */
public enum PublicacaoTipo {
    
    CONHECIMENTO("Conhecimento"),
    DUVIDA("Dúvida");
    
    private final String descricao;

    private PublicacaoTipo(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}

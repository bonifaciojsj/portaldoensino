package br.com.portaldoensino.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jose.bonifacio
 */
@Entity
@Table(name = "mensagemchat")
public class MensagemChat implements PersistentEntity<Long>, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataEnvio;
    @NotNull
    @ManyToOne
    private Usuario usuarioEnvio;
    @NotNull
    @ManyToOne
    private Usuario usuarioRecebimento;
    @Size(max = 1000)
    private String mensagem;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
    
    public Date getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(Date dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    public Usuario getUsuarioEnvio() {
        return usuarioEnvio;
    }

    public void setUsuarioEnvio(Usuario usuarioEnvio) {
        this.usuarioEnvio = usuarioEnvio;
    }

    public Usuario getUsuarioRecebimento() {
        return usuarioRecebimento;
    }

    public void setUsuarioRecebimento(Usuario usuarioRecebimento) {
        this.usuarioRecebimento = usuarioRecebimento;
    }
   
    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
    
    public boolean isNew() {
        return this.id == null;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MensagemChat other = (MensagemChat) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
                   
}

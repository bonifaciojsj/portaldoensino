package br.com.portaldoensino.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author jose.bonifacio
 */
@Entity
@Table(name = "usuario_amigo")
public class UsuarioAmigo implements Serializable, PersistentEntity<Long>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Usuario usuario;
    @ManyToOne
    private Usuario amigo;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataUltimaVisualizacao;
    private Integer mensagensNaoVisualizadas;

    public UsuarioAmigo() {
        mensagensNaoVisualizadas = 0;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Usuario getAmigo() {
        return amigo;
    }

    public void setAmigo(Usuario amigo) {
        this.amigo = amigo;
    }

    public Date getDataUltimaVisualizacao() {
        return dataUltimaVisualizacao;
    }

    public void setDataUltimaVisualizacao(Date dataUltimaVisualizacao) {
        this.dataUltimaVisualizacao = dataUltimaVisualizacao;
    }

    public Integer getMensagensNaoVisualizadas() {
        return mensagensNaoVisualizadas;
    }

    public void setMensagensNaoVisualizadas(Integer mensagensNaoVisualizadas) {
        this.mensagensNaoVisualizadas = mensagensNaoVisualizadas;
    }

    @Override
    public boolean isNew() {
        return id == null;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UsuarioAmigo other = (UsuarioAmigo) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}

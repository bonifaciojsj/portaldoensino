package br.com.portaldoensino.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author BONIFACIO
 */
@Entity
@Table(name = "instituicao")
public class Instituicao implements PersistentEntity<Long>, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String nome;
    private String descricao;
    @OneToMany(mappedBy = "instituicao",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private List<Curso> cursos;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "instituicao_administrador", joinColumns = {
        @JoinColumn(name = "instituicao_id")}, inverseJoinColumns = {
        @JoinColumn(name = "administrador_id")})
    private List<Usuario> administradores;
    private boolean possuiAva;
    @Enumerated
    private InstituicaoTipoAva tipoAva;
    private String tokenAcesso;
    private String enderecoAva;

    public Instituicao() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Usuario> getAdministradores() {
        return administradores;
    }

    public void setAdministradores(List<Usuario> administradores) {
        this.administradores = administradores;
    }
    
    public Set<Usuario> getAdministradoresSet() {
        Set<Usuario> administradoresList = new HashSet<>();
        if (administradores != null && !administradores.isEmpty()) {
            for (Usuario usuario : administradores) {
                administradoresList.add(usuario);
            }
        }
        return administradoresList;
    }

    public boolean isPossuiAva() {
        return possuiAva;
    }

    public void setPossuiAva(boolean possuiAva) {
        this.possuiAva = possuiAva;
    }

    public List<Curso> getCursos() {
        return cursos;
    }

    public Set<Curso> getCursosSet() {
        Set<Curso> cursosSet = new HashSet<>();
        if (cursos != null && !cursos.isEmpty()) {
            for (Curso curso : this.cursos) {
                cursosSet.add(curso);
            }
        }
        return cursosSet;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public InstituicaoTipoAva getTipoAva() {
        return tipoAva;
    }

    public void setTipoAva(InstituicaoTipoAva tipoAva) {
        this.tipoAva = tipoAva;
    }

    public String getTokenAcesso() {
        return tokenAcesso;
    }

    public void setTokenAcesso(String tokenAcesso) {
        this.tokenAcesso = tokenAcesso;
    }

    public String getEnderecoAva() {
        return enderecoAva;
    }

    public void setEnderecoAva(String enderecoAva) {
        this.enderecoAva = enderecoAva;
    }

    public List<Usuario> getAlunos() {
        List<Usuario> alunos = new ArrayList<>();
        for (Curso curso : cursos) {
            for (Usuario alunoCurso : curso.getAlunos()) {
                if (!alunos.contains(alunoCurso)) {
                    alunos.add(alunoCurso);
                }
            }
        }
        return alunos;
    }

    public List<Usuario> getProfessores() {
        List<Usuario> professores = new ArrayList<>();
        for (Curso curso : cursos) {
            for (Usuario professorCurso : curso.getProfessores()) {
                if (!professores.contains(professorCurso)) {
                    professores.add(professorCurso);
                }
            }
        }
        return professores;
    }

    public Set<Usuario> getAdministradores2() {
        Set<Usuario> administradores2 = new HashSet<>();
        administradores2.addAll(administradores);
        return administradores2;
    }

    public boolean ehAdministrador(Usuario usuario) {
        if (usuario == null || administradores == null) {
            return false;
        }
        return administradores.contains(usuario);
    }

    public String getUrlInstituicao() {
        return "<a href=\"http://localhost:8080/PortalDoEnsino/faces/instituicao/instituicao.xhtml?instituicaoId=" + id + "\">" + getNome() + "</a>";
    }
    
    public String getUrlInstituicao(Instituicao instituicao) {
        return "<a href=\"http://localhost:8080/PortalDoEnsino/faces/instituicao/instituicao.xhtml?instituicaoId=" + instituicao.getId() + "\">" + instituicao.getNome() + "</a>";
    }

    @Override
    public boolean isNew() {
        return this.id == null;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Instituicao other = (Instituicao) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }

}

package br.com.portaldoensino.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author BONIFACIO
 */
@Entity
@Table(name = "grupo")
public class Grupo implements PersistentEntity<Long>, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String nome;
    private String descricao;
    @NotNull
    @Enumerated(EnumType.STRING)
    private GrupoTipo tipo;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "grupo_tema", joinColumns = {
        @JoinColumn(name = "grupo_id")}, inverseJoinColumns = {
        @JoinColumn(name = "tema_id")})
    private List<Tema> temas;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "grupo_administrador", joinColumns = {
        @JoinColumn(name = "grupo_id")}, inverseJoinColumns = {
        @JoinColumn(name = "administrador_id")})
    private List<Usuario> administradores;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "grupo_usuario", joinColumns = {
        @JoinColumn(name = "grupo_id")}, inverseJoinColumns = {
        @JoinColumn(name = "usuario_id")})
    private List<Usuario> membros;

    public Grupo() {
        tipo = GrupoTipo.PRIVADO;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public GrupoTipo getTipo() {
        return tipo;
    }

    public void setTipo(GrupoTipo tipo) {
        this.tipo = tipo;
    }

    public List<Tema> getTemas() {
        return temas;
    }

    public void setTemas(List<Tema> temas) {
        this.temas = temas;
    }

    public List<Usuario> getAdministradores() {
        return administradores;
    }

    public void setAdministradores(List<Usuario> administradores) {
        this.administradores = administradores;
    }

    public List<Usuario> getMembros() {
        return membros;
    }

    public void setMembros(List<Usuario> membros) {
        this.membros = membros;
    }

    public boolean isPublico() {
        return tipo.equals(GrupoTipo.PUBLICO);
    }

    public boolean isPrivado() {
        return tipo.equals(GrupoTipo.PRIVADO);
    }

    public boolean ehAdministrador(Usuario usuario) {
        if (usuario == null) {
            return false;
        }
        return administradores.contains(usuario);
    }

    public boolean ehMembro(Usuario usuario) {
        if (usuario == null) {
            return false;
        }
        return membros.contains(usuario);
    }

    public String getUrlGrupo() {
        return "<a href=\"http://localhost:8080/PortalDoEnsino/faces/grupo/grupo.xhtml?grupoId=" + id + "\">" + getNome() + "</a>";
    }
    
    public String getUrlGrupo(Grupo grupo) {
        return "<a href=\"http://localhost:8080/PortalDoEnsino/faces/grupo/grupo.xhtml?grupoId=" + grupo.getId() + "\">" + grupo.getNome() + "</a>";
    }
    
    @Override
    public boolean isNew() {
        return this.id == null;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Grupo other = (Grupo) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }

}

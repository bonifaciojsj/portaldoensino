package br.com.portaldoensino.entity;

import br.com.portaldoensino.util.SenhaUtil;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jose.bonifacio
 */
@Entity
@Table(name = "usuario")
public class Usuario implements PersistentEntity<Long>, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String email;
    @NotNull
    private String senha;
    @NotNull
    private String nome;
    private String sobrenome;
    private String avatar;
    @NotNull
    private Boolean ativo;
    @OneToMany(mappedBy = "usuario",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private List<UsuarioAmigo> amigos;
    @OneToMany(mappedBy = "usuario",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private List<UsuarioSeguidor> usuariosSeguidos;
    @OneToMany(mappedBy = "tema",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<TemaFavorito> temasFavoritos;
    @OneToMany(mappedBy = "usuario",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private List<InstituicaoSeguida> instituicoesSeguidas;

    public Usuario() {
        ativo = true;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = SenhaUtil.getEncryptedPassword(senha);
    }

    public void setSenhaCriptografada(String senhaCriptografada) {
        this.senha = senhaCriptografada;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public String getNomeCompleto() {
        return this.nome.concat(" ").concat(this.sobrenome);
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatarUrl() {
        if (avatar == null || avatar.isEmpty()) {
            return "http://localhost:8080/PortalDoEnsino/faces/download?caminhoArquivo="
                    + "C:/PortalDoEnsino/portaldoensino/PortalDoEnsino/src/main/webapp/resources/images/defaultavatar2.png";
        }
        return "http://localhost:8080/PortalDoEnsino/faces/download?caminhoArquivo=" + avatar;
    }

    public List<UsuarioAmigo> getAmigos() {
        return amigos;
    }

    public boolean ehAmigo(Usuario usuario) {
        boolean ehAmigo = false;
        if (usuario == null) {
            return false;
        }
        for (UsuarioAmigo amigo : amigos) {
            if (amigo.getAmigo().getId().equals(usuario.getId())) {
                ehAmigo = true;
            }
        }
        return ehAmigo;
    }

    public boolean ehSeguido(Usuario usuario) {
        boolean ehSeguido = false;
        if (usuario == null) {
            return false;
        }
        for (UsuarioSeguidor usuarioSeguido : usuariosSeguidos) {
            if (usuarioSeguido.getSeguidor().getId().equals(usuario.getId())) {
                ehSeguido = true;
            }
        }
        return ehSeguido;
    }

    public void setAmigos(List<UsuarioAmigo> amigos) {
        this.amigos = amigos;
    }

    public List<UsuarioSeguidor> getUsuariosSeguidos() {
        return usuariosSeguidos;
    }

    public void setUsuariosSeguidos(List<UsuarioSeguidor> usuariosSeguidos) {
        this.usuariosSeguidos = usuariosSeguidos;
    }

    public List<TemaFavorito> getTemasFavoritos() {
        return temasFavoritos;
    }

    public void setTemasFavoritos(List<TemaFavorito> temasFavoritos) {
        this.temasFavoritos = temasFavoritos;
    }

    public List<InstituicaoSeguida> getInstituicoesSeguidas() {
        return instituicoesSeguidas;
    }

    public void setInstituicoesSeguidas(List<InstituicaoSeguida> instituicoesSeguidas) {
        this.instituicoesSeguidas = instituicoesSeguidas;
    }

    public String getUrlPerfil() {
        return "<a href=\"http://localhost:8080/PortalDoEnsino/faces/usuario/visualizar.xhtml?usuarioId=" + id + "\">" + getNomeCompleto() + "</a>";
    }

    public String getUrlPerfil(Usuario usuario) {
        return "<a href=\"http://localhost:8080/PortalDoEnsino/faces/usuario/visualizar.xhtml?usuarioId=" + usuario.getId() + "\">" + usuario.getNomeCompleto() + "</a>";
    }

    @Override
    public boolean isNew() {
        return this.id == null;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }

}

package br.com.portaldoensino.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jose.bonifacio
 */
@Entity
@Table(name = "curso")
public class Curso implements PersistentEntity<Long>, Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String nome;
    private String descricao;
    @ManyToOne
    private Instituicao instituicao;    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "aluno_curso", joinColumns = {
        @JoinColumn(name = "curso_id")}, inverseJoinColumns = {
        @JoinColumn(name = "aluno_id")})
    private List<Usuario> alunos;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "professor_curso", joinColumns = {
        @JoinColumn(name = "curso_id")}, inverseJoinColumns = {
        @JoinColumn(name = "professor_id")})
    private List<Usuario> professores;
    private String identificadorCursoAva;
    @Transient
    private boolean modoEdicao;

    public Curso() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Instituicao getInstituicao() {
        return instituicao;
    }

    public void setInstituicao(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    public List<Usuario> getAlunos() {
        return alunos;
    }

    public void setAlunos(List<Usuario> alunos) {
        this.alunos = alunos;
    }

    public List<Usuario> getProfessores() {
        return professores;
    }

    public void setProfessores(List<Usuario> professores) {
        this.professores = professores;
    }
    
    public void setModoEdicao(boolean modoEdicao) {
        this.modoEdicao = modoEdicao;
    }
    
    public boolean isModoEdicao() {
        return this.modoEdicao;
    }

    public String getIdentificadorCursoAva() {
        return identificadorCursoAva;
    }

    public void setIdentificadorCursoAva(String identificadorCursoAva) {
        this.identificadorCursoAva = identificadorCursoAva;
    }
    
    public boolean ehProfessor(Usuario usuario) {
        if (usuario == null || professores == null) {
            return false;
        }
        for (Usuario professor : professores) {
            if (professor.equals(usuario)) {
                return true;
            }
        }
        return false;
    }
    
    public Set<Usuario> getAlunosSet() {
        Set<Usuario> usuariosList = new HashSet<>(); 
        if (alunos != null && !alunos.isEmpty()) {
            for (Usuario usuario : alunos) {
                usuariosList.add(usuario);
            }
        }
        return usuariosList;
    }
    
    public Set<Usuario> getProfessoresSet() {        
        Set<Usuario> usuariosList = new HashSet<>(); 
        if (professores != null && !professores.isEmpty()) {
            for (Usuario usuario : professores) {
                usuariosList.add(usuario);
            }
        }
        return usuariosList;
    }
 
    public String getUrlCurso() {
        return "<a href=\"http://localhost:8080/PortalDoEnsino/faces/instituicao/curso.xhtml?cursoId=" + id + "\">" + getNome() + "</a>";
    }
    
    @Override
    public boolean isNew() {
        return this.id == null;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Curso other = (Curso) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }
    
}

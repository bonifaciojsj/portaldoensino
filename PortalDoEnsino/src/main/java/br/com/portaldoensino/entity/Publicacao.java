package br.com.portaldoensino.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jose.bonifacio
 */
@Entity
@Table(name = "publicacao")
public class Publicacao implements PersistentEntity<Long>, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String titulo;
    private String conteudo;
    @Enumerated(EnumType.STRING)
    private PublicacaoTipo tipoPublicacao;
    @NotNull
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataPublicacao;
    @NotNull
    @ManyToOne
    private Usuario usuarioCadastro;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "publicacao_tema", joinColumns = {
        @JoinColumn(name = "publicacao_id")}, inverseJoinColumns = {
        @JoinColumn(name = "tema_id")})
    private List<Tema> temas;
    @ManyToOne
    private Publicacao respostaCorreta;
    @ManyToOne
    private Publicacao publicacaoPai;
    @OneToMany(mappedBy = "publicacaoPai",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private List<Publicacao> comentarios;
    @OneToMany(mappedBy = "publicacao",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private List<PublicacaoAprovar> aprovacoes;
    @ManyToOne
    private Grupo grupo;
    @ManyToOne
    private Instituicao instituicao;
    @ManyToOne
    private Curso curso;
    @Transient
    private boolean modoEdicao;
    @Transient
    private boolean visualizandoComentarios;
    @Transient
    private boolean publicacaoAprovada;

    public Publicacao() {
    }

    public Publicacao(String titulo, String conteudo, PublicacaoTipo tipoPublicacao, List<Tema> temas) {
        this.titulo = titulo;
        this.conteudo = conteudo;
        this.tipoPublicacao = tipoPublicacao;
        this.temas = temas;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

    public PublicacaoTipo getTipoPublicacao() {
        return tipoPublicacao;
    }

    public void setTipoPublicacao(PublicacaoTipo tipoPublicacao) {
        this.tipoPublicacao = tipoPublicacao;
    }

    public Usuario getUsuarioCadastro() {
        return usuarioCadastro;
    }

    public void setUsuarioCadastro(Usuario usuarioCadastro) {
        this.usuarioCadastro = usuarioCadastro;
    }

    public Date getDataPublicacao() {
        return dataPublicacao;
    }

    public void setDataPublicacao(Date dataPublicacao) {
        this.dataPublicacao = dataPublicacao;
    }

    public List<Tema> getTemas() {
        return temas;
    }

    public void setTemas(List<Tema> temas) {
        this.temas = temas;
    }

    public Publicacao getRespostaCorreta() {
        return respostaCorreta;
    }

    public void setRespostaCorreta(Publicacao respostaCorreta) {
        this.respostaCorreta = respostaCorreta;
    }   

    public Publicacao getPublicacaoPai() {
        return publicacaoPai;
    }

    public void setPublicacaoPai(Publicacao publicacaoPai) {
        this.publicacaoPai = publicacaoPai;
    }

    public List<Publicacao> getComentarios() {
        return comentarios;
    }

    public void setComentarios(List<Publicacao> comentarios) {
        this.comentarios = comentarios;
    }

    public List<PublicacaoAprovar> getAprovacoes() {
        return aprovacoes;
    }

    public void setAprovacoes(List<PublicacaoAprovar> aprovacoes) {
        this.aprovacoes = aprovacoes;
    }

    public boolean isAprovada(Usuario usuario) {
        boolean publicacaoAprovada = false;
        publicacaoAprovada = isPublicacaoAprovada();
        for (PublicacaoAprovar aprovacao : aprovacoes) {
            if (aprovacao.getUsuario() != null && aprovacao.getUsuario().equals(usuario)) {
                publicacaoAprovada = true;
            }
        }
        return publicacaoAprovada;
    }

    public boolean isPublicacaoAprovada() {
        return publicacaoAprovada;
    }

    public void setPublicacaoAprovada(boolean publicacaoAprovada) {
        this.publicacaoAprovada = publicacaoAprovada;
    }

    public String getQuantidadeAprovacoes() {
        return String.valueOf(aprovacoes.size());
    }

    public String getQuantidadeComentarios() {
        return String.valueOf(comentarios.size());
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }
    
    public String getTemasVisualizacao() {
        String temasVisualizacao = "";
        for (Tema tema : getTemas()) {
            if (!temasVisualizacao.isEmpty()) {
                temasVisualizacao += "; ";
            }
            temasVisualizacao += tema.getNome();
        }
        return temasVisualizacao;
    }

    public boolean isModoEdicao() {
        return modoEdicao;
    }

    public void setModoEdicao(boolean modoEdicao) {
        this.modoEdicao = modoEdicao;
    }

    public boolean isVisualizandoComentarios() {
        return visualizandoComentarios;
    }

    public void setVisualizandoComentarios(boolean visualizandoComentarios) {
        this.visualizandoComentarios = visualizandoComentarios;
    }

    public boolean isRespondido() {
        return this.respostaCorreta != null;
    }

    public boolean getConhecimento() {
        if (tipoPublicacao != null) {
            return tipoPublicacao.equals(PublicacaoTipo.CONHECIMENTO);
        }
        return false;
    }

    public boolean getDuvida() {
        if (tipoPublicacao != null) {
            return tipoPublicacao.equals(PublicacaoTipo.DUVIDA);
        }
        return false;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public Instituicao getInstituicao() {
        return instituicao;
    }

    public void setInstituicao(Instituicao instituicao) {
        this.instituicao = instituicao;
    }
    
    @PrePersist
    public void execute() {
        dataPublicacao = new Date();
    }

    @Override
    public boolean isNew() {
        return this.id == null;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Publicacao other = (Publicacao) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }

}

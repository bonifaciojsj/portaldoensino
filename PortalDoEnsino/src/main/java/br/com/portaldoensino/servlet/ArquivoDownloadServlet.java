
package br.com.portaldoensino.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jose.bonifacio
 */
@WebServlet(name = "FileDownloadServlet", urlPatterns = "/download")
public class ArquivoDownloadServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

         String caminhoArquivo = request.getParameter("caminhoArquivo");
         String fileName = caminhoArquivo.substring(caminhoArquivo.lastIndexOf("/") + 1);
         String fileType = caminhoArquivo.substring(caminhoArquivo.lastIndexOf(".") + 1);

         response.setContentType(fileType);

         response.setHeader("Content-disposition","attachment; " + fileName);

         File file = new File(caminhoArquivo);

         OutputStream out = response.getOutputStream();
         FileInputStream in = new FileInputStream(file);
         byte[] buffer = new byte[4096];
         int length;
         while ((length = in.read(buffer)) > 0){
            out.write(buffer, 0, length);
         }
         in.close();
         out.flush();
    }
}

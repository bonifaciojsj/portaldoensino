package br.com.portaldoensino.servlet;

import br.com.portaldoensino.entity.CursoMembroAva;
import br.com.portaldoensino.service.NotificacaoService;
import br.com.portaldoensino.sessao.SessionController;
import java.io.IOException;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author BONIFACIO
 */
@WebServlet(name = "MoodleLoginServlet", urlPatterns = "/moodle")
public class MoodleLoginServlet extends HttpServlet {

    @Inject
    private SessionController session;
    @Inject
    private NotificacaoService notificacaoService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        Long avaId = Long.valueOf(request.getParameter("avaId"));
        CursoMembroAva ava = notificacaoService.buscarAvaPorId(avaId);
        String avaUrl = ava.getCurso().getInstituicao().getEnderecoAva();

        out.println("<html>\n"
                + "	<form action=\"" + avaUrl + "/login/index.php\" method=\"post\" name=\"form\" id=\"form\">\n"
                + "  		<p><input type=\"hidden\" name=\"username\" value=\"" + session.getUsuarioLogado().getEmail() + "\" size=\"15\" /></p>\n"
                + "  		<p><input type=\"hidden\" name=\"password\" value=\"" + ava.getMoodlePassword() + "\" size=\"15\" /></p>\n"
                + "	</form>\n"
                + "	<script>\n"
                + "		window.onload = function(){\n"
                + "  			document.forms['form'].submit()\n"
                + "\n"
                + "		}\n"
                + "	</script>\n"
                + "</html>");
    }
}

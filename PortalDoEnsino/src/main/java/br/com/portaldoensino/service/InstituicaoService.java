package br.com.portaldoensino.service;

import br.com.portaldoensino.dao.DAO;
import br.com.portaldoensino.entity.Instituicao;
import br.com.portaldoensino.entity.InstituicaoSeguida;
import br.com.portaldoensino.entity.InstituicaoSeguidaPK;
import br.com.portaldoensino.entity.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author BONIFACIO
 */
@Stateless
public class InstituicaoService implements Serializable {

    @Inject
    private DAO<Instituicao, Long> instituicaoDAO;
    @Inject
    private DAO<InstituicaoSeguida, InstituicaoSeguidaPK> instituicaoSeguidaDAO;

    public Instituicao salvar(Instituicao instituicao) {
        return instituicaoDAO.save(instituicao);
    }

    public void excluir(Instituicao instituicao) {
        instituicaoDAO.remove(instituicao);
    }

    public Instituicao buscarPorId(Long id) {
        return instituicaoDAO.findByPrimaryKey(id);
    }

    public List<Instituicao> buscarInstituicoesParticipadas(Usuario usuario) {
        String query = "SELECT DISTINCT instituicao FROM " + Instituicao.class.getSimpleName() + " instituicao"
                + " LEFT JOIN instituicao.cursos curso"
                + " LEFT JOIN instituicao.administradores administrador"
                + " LEFT JOIN curso.alunos aluno"
                + " LEFT JOIN curso.professores professor"
                + " WHERE aluno.id = " + usuario.getId()
                + "    OR professor.id = " + usuario.getId()
                + "    OR administrador.id = " + usuario.getId();
        return instituicaoDAO.findByQuery(query);
    }
    
    public List<InstituicaoSeguida> buscarInstituicoesSeguidas(Usuario usuario) {
        String query = "SELECT instituicaoSeguida FROM " + InstituicaoSeguida.class.getSimpleName() + " instituicaoSeguida"
                + "      WHERE instituicaoSeguida.usuario.id = " + usuario.getId();
        return instituicaoSeguidaDAO.findByQuery(query);
    }
    
    public List<Instituicao> buscarPorString(String string) {
        String query = "SELECT DISTINCT instituicao FROM " + Instituicao.class.getSimpleName() + " instituicao"
                + "      WHERE instituicao.nome LIKE '%" + string + "%'";
        return instituicaoDAO.findByQuery(query);
    }
    
        public void marcarInstituicaoSeguida(Usuario usuario, Instituicao instituicao) {
        InstituicaoSeguidaPK instituicaoSeguidaPK = new InstituicaoSeguidaPK(usuario, instituicao);
        InstituicaoSeguida instituicaoSeguida = new InstituicaoSeguida();
        instituicaoSeguida.setId(instituicaoSeguidaPK);
        instituicaoSeguidaDAO.save(instituicaoSeguida);
    }

    public void desmarcarInstituicaoSeguida(Usuario usuario, Instituicao instituicao) {
        InstituicaoSeguidaPK instituicaoSeguidaPK = new InstituicaoSeguidaPK(usuario, instituicao);
        InstituicaoSeguida instituicaoSeguida = new InstituicaoSeguida();
        instituicaoSeguida.setId(instituicaoSeguidaPK);
        String query = "DELETE FROM " + InstituicaoSeguida.class.getSimpleName() + " instituicaoSeguida"
                + " WHERE instituicaoSeguida.usuario.id = " + usuario.getId()
                + "   AND instituicaoSeguida.instituicao.id = " + instituicao.getId();
        instituicaoSeguidaDAO.executeUpdate(query);
    }

}

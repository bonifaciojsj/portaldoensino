package br.com.portaldoensino.service;

import br.com.portaldoensino.dao.DAO;
import br.com.portaldoensino.entity.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class PublicacaoService implements Serializable {

    @Inject
    private UsuarioService usuarioService;
    @Inject
    private CursoService cursoService;
    @Inject
    private DAO<Publicacao, Long> publicacaoDAO;
    @Inject
    private DAO<PublicacaoAprovar, PublicacaoAprovarPK> publicacaoAprovarDAO;
    
    public Publicacao buscarPorId(Long id) {
        return publicacaoDAO.findByPrimaryKey(id);
    }

    public Publicacao salvar(Publicacao publicacao) {
        return publicacaoDAO.save(publicacao);
    }

    public void excluir(Publicacao publicacao) {
        publicacaoDAO.remove(publicacao);
    }

    public Publicacao buscarPorIdFetchTemas(Long id) {
        String query = "SELECT DISTINCT publicacao FROM " + Publicacao.class.getSimpleName() + " publicacao"
                + " LEFT JOIN FETCH publicacao.temas tema"
                + " WHERE publicacao.id = " + String.valueOf(id);
        return publicacaoDAO.findByQuery(query).get(0);
    }

    public Publicacao buscarPorIdFetchComentarios(Long id) {
        String query = "SELECT DISTINCT publicacao FROM " + Publicacao.class.getSimpleName() + " publicacao"
                + " LEFT JOIN FETCH publicacao.comentarios comentario"
                + " WHERE publicacao.id = " + String.valueOf(id);
        return publicacaoDAO.findByQuery(query).get(0);
    }

    public List<Publicacao> buscarPublicacoesPorTemasFetchTemas(List<Tema> temas) {
        String query = "SELECT DISTINCT publicacao FROM " + Publicacao.class.getSimpleName() + " publicacao"
                + " LEFT JOIN publicacao.temas tema"
                + " WHERE tema IN (" + temas.toString() + ")"
                + " AND publicacao.grupo IS NULL";
        List<Publicacao> publicacoes = publicacaoDAO.findByQuery(query);
        if (!publicacoes.isEmpty()) {
            return buscarPublicacoesFetchTemas(publicacoes);
        }
        return new ArrayList<>();
    }
    
    public List<Publicacao> buscarPublicacoesPorString(String string) {
        String query = "SELECT DISTINCT publicacao FROM " + Publicacao.class.getSimpleName() + " publicacao"
                + "  LEFT JOIN publicacao.temas tema"
                + "  LEFT JOIN publicacao.grupo grupo"
                + "  LEFT JOIN publicacao.curso curso"
                + "  LEFT JOIN publicacao.instituicao instituicao"
                + "      WHERE publicacao.usuarioCadastro.nome LIKE '%" + string + "%'"
                + "         OR publicacao.usuarioCadastro.sobrenome LIKE '%" + string + "%'"
                + "         OR tema.nome LIKE '%" + string + "%'"
                + "         OR curso.nome LIKE '%" + string + "%'"
                + "         OR instituicao.nome LIKE '%" + string +  "%'"
                + "         OR grupo.nome LIKE '%" + string + "%'"
                + "         OR publicacao.titulo LIKE '%" + string + "%'"
                + "   ORDER BY publicacao.dataPublicacao DESC";
        return publicacaoDAO.findByQuery(query);
    }

    public List<Publicacao> buscarPublicacoesIndex(Usuario usuario, List<Grupo> gruposUsuario) {
        String query;
        if (usuario.isNew()) {
            query = "      SELECT publicacao FROM " + Publicacao.class.getSimpleName() + " publicacao"
                    + "     WHERE publicacao.publicacaoPai IS NULL"
                    + "       AND publicacao.grupo IS NULL"
                    + "       AND publicacao.instituicao IS NULL"
                    + "       AND publicacao.curso IS NULL"
                    + "  ORDER BY publicacao.dataPublicacao DESC";
        } else {
            usuario = usuarioService.fetchTemas(usuario);
            List<Usuario> amigos = new ArrayList<>();
            List<Usuario> usuariosSeguidos = new ArrayList<>();
            List<Tema> temasFavoritos = new ArrayList<>();
            List<Instituicao> instituicoesSeguidas = new ArrayList<>();
            List<Curso> cursosParticipados = new ArrayList<>();
            for (UsuarioAmigo ua : usuario.getAmigos()) {
                amigos.add(ua.getAmigo());
            }
            for (UsuarioSeguidor us : usuario.getUsuariosSeguidos()) {
                usuariosSeguidos.add(us.getSeguidor());
            }
            for (TemaFavorito tf : usuario.getTemasFavoritos()) {
                temasFavoritos.add(tf.getTema());
            }
            for (InstituicaoSeguida is : usuario.getInstituicoesSeguidas()) {
                instituicoesSeguidas.add(is.getInstituicao());
            }
            for (Curso curso : cursoService.buscarCursosParticipados(usuario)) {
                cursosParticipados.add(curso);
            }
            String whereAdicional = (!gruposUsuario.isEmpty() ? "publicacao.grupo.id IN (" + gruposUsuario.toString() + ")" : "");
            whereAdicional += (!temasFavoritos.isEmpty() ? (whereAdicional.isEmpty() ? "" : " OR ") + "tema.id IN (" + temasFavoritos.toString() + ")" : "");
            whereAdicional += (!amigos.isEmpty() ? (whereAdicional.isEmpty() ? "" : " OR ") + " publicacao.usuarioCadastro.id IN (" + amigos.toString() + ")" : "");
            whereAdicional += (!usuariosSeguidos.isEmpty() ? (whereAdicional.isEmpty() ? "" : " OR ") + " publicacao.usuarioCadastro.id IN (" + usuariosSeguidos.toString() + ")" : "");
            whereAdicional += (!instituicoesSeguidas.isEmpty() ? (whereAdicional.isEmpty() ? "" : " OR ") + " publicacao.instituicao.id IN (" + instituicoesSeguidas.toString() + ")" : "");
            whereAdicional += (!cursosParticipados.isEmpty() ? (whereAdicional.isEmpty() ? "" : " OR ") + " publicacao.curso.id IN (" + cursosParticipados.toString() + ")" : "");
            
            query = "      SELECT DISTINCT publicacao FROM " + Publicacao.class.getSimpleName() + " publicacao"
                    + " LEFT JOIN publicacao.temas tema"
                    + "     WHERE publicacao.publicacaoPai IS NULL"
                    + (!whereAdicional.isEmpty() ? " AND (" + whereAdicional + ""
                    + " OR publicacao.usuarioCadastro.id = " + usuario.getId() + ")" : "")
                    + " ORDER BY publicacao.dataPublicacao DESC";
        }
        return publicacaoDAO.findByQuery(query);
    }

    public List<Publicacao> buscarPublicacoesPorGrupo(Grupo grupo) {
        String query = "SELECT DISTINCT publicacao FROM " + Publicacao.class.getSimpleName() + " publicacao"
                + " LEFT JOIN publicacao.grupo grupo"
                + " WHERE grupo.id = " + grupo.getId();
        List<Publicacao> publicacoes = publicacaoDAO.findByQuery(query);
        if (!publicacoes.isEmpty()) {
            return buscarPublicacoesFetchTemas(publicacoes);
        }
        return new ArrayList<>();
    }

    public List<Publicacao> buscarPublicacoesPorInstituicao(Instituicao instituicao) {
        String query = "SELECT DISTINCT publicacao FROM " + Publicacao.class.getSimpleName() + " publicacao"
                + " LEFT JOIN publicacao.instituicao instituicao"
                + " WHERE instituicao.id = " + instituicao.getId();
        List<Publicacao> publicacoes = publicacaoDAO.findByQuery(query);
        if (!publicacoes.isEmpty()) {
            return buscarPublicacoesFetchTemas(publicacoes);
        }
        return new ArrayList<>();
    }

    public List<Publicacao> buscarPublicacoesPorCurso(Curso curso) {
        String query = "SELECT DISTINCT publicacao FROM " + Publicacao.class.getSimpleName() + " publicacao"
                + " LEFT JOIN publicacao.curso curso"
                + " WHERE curso.id = " + curso.getId();
        List<Publicacao> publicacoes = publicacaoDAO.findByQuery(query);
        if (!publicacoes.isEmpty()) {
            return buscarPublicacoesFetchTemas(publicacoes);
        }
        return new ArrayList<>();
    }

    public List<Publicacao> buscarPublicacoesFetchTemas(List<Publicacao> publicacoes) {
        String query = "SELECT DISTINCT publicacao FROM " + Publicacao.class.getSimpleName() + " publicacao"
                + " LEFT JOIN FETCH publicacao.temas tema"
                + " WHERE publicacao IN (" + publicacoes.toString() + ")"
                + " ORDER BY publicacao.dataPublicacao DESC";
        return publicacaoDAO.findByQuery(query);
    }

    public List<Publicacao> buscarPublicacoesPorUsuarioFetchTemas(Usuario usuario) {
        String query = "SELECT DISTINCT publicacao FROM " + Publicacao.class.getSimpleName() + " publicacao"
                + " WHERE publicacao.usuarioCadastro.id = " + usuario.getId()
                + " AND publicacao.publicacaoPai IS NULL"
                + " AND publicacao.grupo IS NULL";
        List<Publicacao> publicacoes = publicacaoDAO.findByQuery(query);
        if (!publicacoes.isEmpty()) {
            return buscarPublicacoesFetchTemas(publicacoes);
        }
        return new ArrayList<>();
    }

    public List<Publicacao> buscarPublicacoesFetchAprovacoes(List<Publicacao> publicacoes) {
        String query = "SELECT DISTINCT publicacao FROM " + Publicacao.class.getSimpleName() + " publicacao"
                + " LEFT JOIN publicacao.aprovacoes aprovacao"
                + " WHERE publicacao IN (" + publicacoes.toString() + ")";
        return publicacaoDAO.findByQuery(query);
    }

    public void aprovarPublicacao(Usuario usuario, Publicacao publicacao) {
        PublicacaoAprovarPK aprovacaoPK = new PublicacaoAprovarPK(usuario, publicacao);
        PublicacaoAprovar aprovacao = new PublicacaoAprovar();
        aprovacao.setId(aprovacaoPK);
        publicacao.getAprovacoes().add(aprovacao);
        publicacao.setPublicacaoAprovada(true);
        publicacaoAprovarDAO.save(publicacao);
    }

    public void desaprovarPublicacao(Usuario usuario, Publicacao publicacao) {
        PublicacaoAprovarPK aprovacaoPK = new PublicacaoAprovarPK(usuario, publicacao);
        PublicacaoAprovar aprovacao = new PublicacaoAprovar();
        aprovacao.setId(aprovacaoPK);
        publicacao.getAprovacoes().remove(aprovacao);
        Iterator<PublicacaoAprovar> it = publicacao.getAprovacoes().iterator();
        while (it.hasNext()) {
            PublicacaoAprovar pa = it.next();
            if (pa.getId().equals(aprovacaoPK)) {
                it.remove();
            }
        }
        publicacaoDAO.save(publicacao);
        publicacaoAprovarDAO.remove(aprovacao);
        publicacao.setPublicacaoAprovada(false);
    }

}

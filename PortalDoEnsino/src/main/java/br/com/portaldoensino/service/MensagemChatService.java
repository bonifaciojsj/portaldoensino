package br.com.portaldoensino.service;

import br.com.portaldoensino.dao.DAO;
import br.com.portaldoensino.entity.MensagemChat;
import br.com.portaldoensino.entity.Usuario;
import br.com.portaldoensino.entity.UsuarioAmigo;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class MensagemChatService implements Serializable {

    @Inject
    private DAO<MensagemChat, Long> mensagemChatDAO;
    @Inject
    private DAO<UsuarioAmigo, Long> usuarioAmigoDAO;

    public MensagemChat salvar(MensagemChat mensagem, UsuarioAmigo amizade) {
        amizade.setMensagensNaoVisualizadas(amizade.getMensagensNaoVisualizadas() + 1);
        System.out.println("teste1: " + amizade.getMensagensNaoVisualizadas());
        System.out.println("teste2: " + amizade.getId());
        amizade = usuarioAmigoDAO.save(amizade);
        System.out.println("teste3: " + amizade.getMensagensNaoVisualizadas());
        return mensagemChatDAO.save(mensagem);
    }

    public List<MensagemChat> buscarMensagensEntreUsuarios(Usuario usuario1, Usuario usuario2) {
        String query = "SELECT mensagem FROM " + MensagemChat.class.getSimpleName() + " mensagem"
                + " WHERE (mensagem.usuarioEnvio.id = " + usuario1.getId()
                + " AND mensagem.usuarioRecebimento.id = " + usuario2.getId() + ")"
                + " OR (mensagem.usuarioEnvio.id = "  + usuario2.getId()
                + " AND mensagem.usuarioRecebimento.id = " + usuario1.getId() + ")"
                + " ORDER BY mensagem.dataEnvio";
        return mensagemChatDAO.findByQuery(query);
    }
}

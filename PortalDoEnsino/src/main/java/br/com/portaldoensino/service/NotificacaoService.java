package br.com.portaldoensino.service;

import br.com.portaldoensino.dao.DAO;
import br.com.portaldoensino.entity.Curso;
import br.com.portaldoensino.entity.CursoMembroAva;
import br.com.portaldoensino.entity.CursoMembroAvaTipo;
import br.com.portaldoensino.entity.Grupo;
import br.com.portaldoensino.entity.Instituicao;
import br.com.portaldoensino.entity.Notificacao;
import br.com.portaldoensino.entity.NotificacaoTipo;
import br.com.portaldoensino.entity.Usuario;
import br.com.portaldoensino.entity.UsuarioAmigo;
import br.com.portaldoensino.util.MoodleUtil;
import br.com.portaldoensino.util.SenhaUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author BONIFACIO
 */
@Stateless
public class NotificacaoService implements Serializable {

    @Inject
    private UsuarioService usuarioService;
    @Inject
    private DAO<Notificacao, Long> notificacaoDAO;
    @Inject
    private DAO<UsuarioAmigo, Long> usuarioAmigoDAO;
    @Inject
    private DAO<Grupo, Long> grupoDAO;
    @Inject
    private DAO<CursoMembroAva, Long> cursoMembroAvaDAO;
    @Inject
    private DAO<Instituicao, Long> instituicaoDAO;
    @Inject
    private DAO<Curso, Long> cursoDAO;
    @Inject
    private DAO<Usuario, Long> usuarioDAO;

    public Notificacao salvar(Notificacao notificacao) {
        switch (notificacao.getNotificacaoTipo()) {
            case PEDIDO_AMIZADE:
                if (notificacao.getNotificacaoAceita()) {
                    UsuarioAmigo amizade1 = new UsuarioAmigo();
                    amizade1.setUsuario(notificacao.getUsuarioNotificado());
                    amizade1.setAmigo(notificacao.getUsuarioEnvio());
                    usuarioAmigoDAO.save(amizade1);
                    gerarNotificacaoDeAmizadeAceita(notificacao);
                }
                break;
            case PEDIDO_AMIZADE_ACEITO:
                UsuarioAmigo amizade1 = new UsuarioAmigo();
                amizade1.setUsuario(notificacao.getUsuarioNotificado());
                amizade1.setAmigo(notificacao.getUsuarioEnvio());
                UsuarioAmigo amizadeBanco = usuarioService.buscarPorUsuarios(notificacao.getUsuarioNotificado(), notificacao.getUsuarioEnvio());
                if (amizadeBanco == null) {
                    usuarioAmigoDAO.save(amizade1);
                } else {
                    UsuarioAmigo amizade2 = new UsuarioAmigo();
                    amizade2.setUsuario(notificacao.getUsuarioEnvio());
                    amizade2.setAmigo(notificacao.getUsuarioNotificado());
                    usuarioAmigoDAO.save(amizade2);
                }
                break;
            case AMIZADE_REMOVIDA:
                UsuarioAmigo amizade = usuarioService.buscarPorUsuarios(notificacao.getUsuarioNotificado(), notificacao.getUsuarioEnvio());
                usuarioAmigoDAO.executeUpdate("DELETE FROM " + UsuarioAmigo.class.getSimpleName() + " where id = " + amizade.getId());
                break;
            case GRUPO_INGRESSO:
                if (notificacao.getNotificacaoAceita()) {
                    Grupo grupo = notificacao.getGrupo();
                    grupo.getMembros().add(notificacao.getUsuarioEnvio());
                    notificacao.setGrupo(null);
                    gerarNotificacaoDeAceitacaoDeIngressoParaGrupo(notificacao, grupo);
                    notificacao.setGrupo(grupo);
                    Set<Usuario> usuariosList = new HashSet<>();
                    usuariosList.addAll(grupo.getMembros());
                    grupo.getMembros().clear();
                    grupo.getMembros().addAll(usuariosList);
                    grupoDAO.save(grupo);
                }
                break;
            case GRUPO_CONVITE_MEMBRO:
                if (notificacao.getNotificacaoAceita()) {
                    Grupo grupo = notificacao.getGrupo();
                    grupo.getMembros().add(notificacao.getUsuarioNotificado());
                    notificacao.setGrupo(null);
                    Set<Usuario> usuariosList = new HashSet<>();
                    usuariosList.addAll(grupo.getMembros());
                    grupo.getMembros().clear();
                    grupo.getMembros().addAll(usuariosList);
                    grupoDAO.save(grupo);
                }
                break;
            case GRUPO_CONVITE_ADMINISTRADOR:
                if (notificacao.getNotificacaoAceita()) {
                    Grupo grupo = notificacao.getGrupo();
                    grupo.getAdministradores().add(notificacao.getUsuarioNotificado());
                    notificacao.setGrupo(null);
                    Set<Usuario> usuariosList = new HashSet<>();
                    usuariosList.addAll(grupo.getAdministradores());
                    grupo.getAdministradores().clear();
                    grupo.getAdministradores().addAll(usuariosList);
                    grupoDAO.save(grupo);
                }
                break;
            case CURSO_INGRESSO_ALUNO:
                if (notificacao.getNotificacaoAceita()) {
                    Curso curso = notificacao.getCurso();
                    curso.getAlunos().add(notificacao.getUsuarioEnvio());
                    notificacao.setCurso(null);
                    gerarNotificacaoDeAceitacaoDeIngressoParaCurso(notificacao, curso);
                    notificacao.setCurso(curso);
                    Set<Usuario> usuariosList = new HashSet<>();
                    usuariosList.addAll(curso.getAlunos());
                    curso.getAlunos().clear();
                    curso.getAlunos().addAll(usuariosList);
                    cursoDAO.save(curso);
                }
                break;
            case CURSO_CONVITE_ALUNO:
                if (notificacao.getNotificacaoAceita()) {
                    Curso curso = notificacao.getCurso();
                    curso.getAlunos().add(notificacao.getUsuarioNotificado());
                    notificacao.setInstituicao(null);
                    Set<Usuario> usuariosList = new HashSet<>();
                    usuariosList.addAll(curso.getAlunos());
                    curso.getAlunos().clear();
                    curso.getAlunos().addAll(usuariosList);
                    cursoDAO.save(curso);
                }
                break;
            case CURSO_CONVITE_PROFESSOR:
                if (notificacao.getNotificacaoAceita()) {
                    Curso curso = notificacao.getCurso();
                    curso.getProfessores().add(notificacao.getUsuarioNotificado());
                    notificacao.setInstituicao(null);
                    Set<Usuario> usuariosList = new HashSet<>();
                    usuariosList.addAll(curso.getProfessores());
                    curso.getProfessores().clear();
                    curso.getProfessores().addAll(usuariosList);
                    cursoDAO.save(curso);
                }
                break;
            case INSTITUICAO_CONVITE_ADMINITRADOR:
                if (notificacao.getNotificacaoAceita()) {
                    Instituicao instituicao = notificacao.getInstituicao();
                    instituicao.getAdministradores().add(notificacao.getUsuarioNotificado());
                    notificacao.setInstituicao(null);
                    Set<Usuario> usuariosList = new HashSet<>();
                    usuariosList.addAll(instituicao.getAdministradores());
                    instituicao.getAdministradores().clear();
                    instituicao.getAdministradores().addAll(usuariosList);
                    instituicaoDAO.save(instituicao);
                }
                break;
            case AVA_CONVITE:
                gerarNotificacaoDeConviteAceitoParaAva(notificacao);
                break;
        }
        return notificacaoDAO.save(notificacao);
    }

    public Notificacao buscarPorId(Long id) {
        return notificacaoDAO.findByPrimaryKey(id);
    }

    public List<Notificacao> buscarTodosPorUsuario(Usuario usuario) {
        String query = "SELECT notificacao FROM " + Notificacao.class.getSimpleName() + " notificacao"
                + " WHERE notificacao.usuarioNotificado.id = " + usuario.getId()
                + " OR EXISTS (SELECT 1 FROM " + Grupo.class.getSimpleName() + " grupo"
                + "         LEFT JOIN grupo.administradores administrador"
                + "             WHERE grupo.id = notificacao.grupo.id"
                + "               AND notificacao.notificacaoTipo = 'GRUPO_INGRESSO'"
                + "               AND administrador.id = " + usuario.getId() + ")"
                + " OR EXISTS (SELECT 1 FROM " + Instituicao.class.getSimpleName() + " instituicao"
                + "         LEFT JOIN instituicao.administradores administrador"
                + "             WHERE instituicao.id = notificacao.instituicao.id"
                + "               AND notificacao.notificacaoTipo = 'CURSO_INGRESSO_ALUNO'"
                + "               AND administrador.id = " + usuario.getId() + ")"
                + " ORDER BY notificacao.id DESC";
        return notificacaoDAO.findByQuery(query);
    }

    public List<Notificacao> buscarTodosNaoFinalizadosPorUsuario(Usuario usuario) {
        String query = "SELECT notificacao FROM " + Notificacao.class.getSimpleName() + " notificacao"
                + " WHERE notificacao.usuarioNotificado.id = " + usuario.getId()
                + " AND notificacao.finalizado = false"
                + " OR EXISTS (SELECT 1 FROM " + Grupo.class.getSimpleName() + " grupo"
                + "         LEFT JOIN grupo.administradores administrador"
                + "             WHERE grupo.id = notificacao.grupo.id"
                + "               AND notificacao.finalizado = false"
                + "               AND notificacao.notificacaoTipo = 'GRUPO_INGRESSO'"
                + "               AND administrador.id = " + usuario.getId() + ")"
                + " OR EXISTS (SELECT 1 FROM " + Instituicao.class.getSimpleName() + " instituicao"
                + "         LEFT JOIN instituicao.administradores administrador"
                + "             WHERE instituicao.id = notificacao.instituicao.id"
                + "               AND notificacao.finalizado = false"
                + "               AND notificacao.notificacaoTipo = 'CURSO_INGRESSO_ALUNO'"
                + "               AND administrador.id = " + usuario.getId() + ")";
        return notificacaoDAO.findByQuery(query);
    }

    public boolean possuiPedidoAmizadePendente(Usuario usuarioEnvio, Usuario usuarioNotificado) {
        String query = "SELECT notificacao FROM " + Notificacao.class.getSimpleName() + " notificacao"
                + " WHERE notificacao.usuarioNotificado.id = " + usuarioNotificado.getId()
                + " AND notificacao.usuarioEnvio.id = " + usuarioEnvio.getId()
                + " AND notificacao.notificacaoAceita IS NULL"
                + " AND notificacao.notificacaoTipo = 'PEDIDO_AMIZADE'";
        List<Notificacao> notificacoes = notificacaoDAO.findByQuery(query);
        return !notificacoes.isEmpty();
    }

    public void gerarNotificacaoDeSolicitacaoDeAmizade(Usuario usuarioEnvio, Usuario usuarioNotificado) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioEnvio(usuarioEnvio);
        notificacao.setUsuarioNotificado(usuarioNotificado);
        notificacao.setDescricao(usuarioEnvio.getUrlPerfil() + " adicionou-o como amigo. Você aceita?");
        notificacao.setNotificacaoTipo(NotificacaoTipo.PEDIDO_AMIZADE);
        notificacao.setFinalizado(Boolean.FALSE);
        notificacaoDAO.save(notificacao);
    }

    public void gerarNotificacaoDeAmizadeRemovida(Usuario usuarioEnvio, Usuario usuarioNotificado) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioEnvio(usuarioEnvio);
        notificacao.setUsuarioNotificado(usuarioNotificado);
        notificacao.setDescricao(usuarioEnvio.getUrlPerfil() + " desfez a amizade com você.");
        notificacao.setNotificacaoTipo(NotificacaoTipo.AMIZADE_REMOVIDA);
        notificacao.setFinalizado(Boolean.FALSE);
        notificacaoDAO.save(notificacao);
    }

    public void gerarNotificacaoDeAmizadeAceita(Notificacao notificacaoAmizade) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioEnvio(notificacaoAmizade.getUsuarioNotificado());
        notificacao.setUsuarioNotificado(notificacaoAmizade.getUsuarioEnvio());
        notificacao.setNotificacaoTipo(NotificacaoTipo.PEDIDO_AMIZADE_ACEITO);
        notificacao.setDescricao(notificacaoAmizade.getUsuarioNotificado().getUrlPerfil() + " aceitou seu pedido de amizade.");
        notificacao.setFinalizado(Boolean.FALSE);
        notificacaoDAO.save(notificacao);
    }

    public void gerarNotificacaoDeSoliciticaoDeIngressoParaGrupo(Usuario usuario, Grupo grupo) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioEnvio(usuario);
        notificacao.setGrupo(grupo);
        notificacao.setNotificacaoTipo(NotificacaoTipo.GRUPO_INGRESSO);
        notificacao.setDescricao(usuario.getUrlPerfil() + " solicitou entrada no grupo " + grupo.getUrlGrupo() + ". Você aceita?");
        notificacao.setFinalizado(Boolean.FALSE);
        notificacaoDAO.save(notificacao);
    }

    public void gerarNotificacaoDeAceitacaoDeIngressoParaGrupo(Notificacao notificacaoIngresso, Grupo grupo) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioNotificado(notificacaoIngresso.getUsuarioEnvio());
        notificacao.setNotificacaoTipo(NotificacaoTipo.GRUPO_INGRESSO_ACEITO);
        notificacao.setDescricao("Sua solicitação de ingresso no grupo " + grupo.getUrlGrupo() + " foi aceita.");
        notificacao.setFinalizado(Boolean.FALSE);
        notificacao = notificacaoDAO.save(notificacao);
        notificacao.setGrupo(grupo);
        notificacaoDAO.save(notificacao);
    }

    public void gerarNotificacaoDeConviteParaMembroDeGrupo(Usuario usuario, Grupo grupo) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioNotificado(usuario);
        notificacao.setGrupo(grupo);
        notificacao.setNotificacaoTipo(NotificacaoTipo.GRUPO_CONVITE_MEMBRO);
        notificacao.setDescricao("Você foi convidado para participar do grupo " + grupo.getUrlGrupo() + ". Você aceita?");
        notificacao.setFinalizado(Boolean.FALSE);
        notificacaoDAO.save(notificacao);
    }

    public void gerarNotificacaoDeConviteParaAdministradorDeGrupo(Usuario usuario, Grupo grupo) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioNotificado(usuario);
        notificacao.setGrupo(grupo);
        notificacao.setNotificacaoTipo(NotificacaoTipo.GRUPO_CONVITE_ADMINISTRADOR);
        notificacao.setDescricao("Você foi convidado para ser administrador do grupo " + grupo.getUrlGrupo() + ". Você aceita?");
        notificacao.setFinalizado(Boolean.FALSE);
        notificacaoDAO.save(notificacao);
    }

    public void gerarNotificacaoDeRemocaoDeMembroDeGrupo(Usuario usuario, Grupo grupo) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioNotificado(usuario);
        notificacao.setGrupo(grupo);
        notificacao.setNotificacaoTipo(NotificacaoTipo.GRUPO_REMOCAO_MEMBRO);
        notificacao.setDescricao("Você foi removido do grupo " + grupo.getUrlGrupo() + ".");
        notificacao.setFinalizado(Boolean.FALSE);
        notificacaoDAO.save(notificacao);
    }

    public void gerarNotificacaoDeRemocaoDeAdministradorDeGrupo(Usuario usuario, Grupo grupo) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioNotificado(usuario);
        notificacao.setGrupo(grupo);
        notificacao.setNotificacaoTipo(NotificacaoTipo.GRUPO_REMOCAO_ADMINISTRADOR);
        notificacao.setDescricao("Você foi removido da administração do grupo " + grupo.getUrlGrupo() + ".");
        notificacao.setFinalizado(Boolean.FALSE);
        notificacaoDAO.save(notificacao);
    }

    public void gerarNotificacaoDeConviteParaAdministradorDeInstituicao(Usuario usuario, Instituicao instituicao) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioNotificado(usuario);
        notificacao.setInstituicao(instituicao);
        notificacao.setNotificacaoTipo(NotificacaoTipo.INSTITUICAO_CONVITE_ADMINITRADOR);
        notificacao.setDescricao("Você foi convidado para ser administrador da instituição " + instituicao.getUrlInstituicao() + ". Você aceita?");
        notificacao.setFinalizado(Boolean.FALSE);
        notificacaoDAO.save(notificacao);
    }

    public void gerarNotificacaoDeRemocaoDeAdministradorDeInstituicao(Usuario usuario, Instituicao instituicao) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioNotificado(usuario);
        notificacao.setInstituicao(instituicao);
        notificacao.setNotificacaoTipo(NotificacaoTipo.INSTITUICAO_REMOCAO_ADMINISTRADOR);
        notificacao.setDescricao("Você foi removido da administração da instituição " + instituicao.getUrlInstituicao() + ".");
        notificacao.setFinalizado(Boolean.FALSE);
        notificacaoDAO.save(notificacao);
    }

    public void gerarNotificacaoDeConviteParaProfessorDeCurso(Usuario usuario, Curso curso) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioNotificado(usuario);
        notificacao.setCurso(curso);
        notificacao.setInstituicao(curso.getInstituicao());
        notificacao.setNotificacaoTipo(NotificacaoTipo.CURSO_CONVITE_PROFESSOR);
        notificacao.setDescricao("Você foi convidado para ser professor do curso " + curso.getUrlCurso() + " na instituição " + curso.getInstituicao().getUrlInstituicao() + ". Você aceita?");
        notificacao.setFinalizado(Boolean.FALSE);
        notificacaoDAO.save(notificacao);
    }

    public void gerarNotificacaoDeRemocaoDeProfesorDeCurso(Usuario usuario, Curso curso) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioNotificado(usuario);
        notificacao.setCurso(curso);
        notificacao.setInstituicao(curso.getInstituicao());
        notificacao.setNotificacaoTipo(NotificacaoTipo.CURSO_REMOCAO_PROFESSOR);
        notificacao.setDescricao("Você foi removido do grupo de professores do curso " + curso.getUrlCurso() + " da instituição " + curso.getInstituicao().getUrlInstituicao() + ".");
        notificacao.setFinalizado(Boolean.FALSE);
        notificacaoDAO.save(notificacao);
    }

    public void gerarNotificacaoDeConviteParaAlunoDeCurso(Usuario usuario, Curso curso) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioNotificado(usuario);
        notificacao.setCurso(curso);
        notificacao.setInstituicao(curso.getInstituicao());
        notificacao.setNotificacaoTipo(NotificacaoTipo.CURSO_CONVITE_ALUNO);
        notificacao.setDescricao("Você foi convidado para ser aluno do curso " + curso.getUrlCurso() + " na instituição " + curso.getInstituicao().getUrlInstituicao() + ". Você aceita?");
        notificacao.setFinalizado(Boolean.FALSE);
        notificacaoDAO.save(notificacao);
    }

    public void gerarNotificacaoDeRemocaoDeAlunoDeCurso(Usuario usuario, Curso curso) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioNotificado(usuario);
        notificacao.setCurso(curso);
        notificacao.setInstituicao(curso.getInstituicao());
        notificacao.setNotificacaoTipo(NotificacaoTipo.CURSO_REMOCAO_ALUNO);
        notificacao.setDescricao("Você foi removido do grupo de alunos do curso " + curso.getUrlCurso() + " da instituição " + curso.getInstituicao().getUrlInstituicao() + ".");
        notificacao.setFinalizado(Boolean.FALSE);
        notificacaoDAO.save(notificacao);
    }

    public void gerarNotificacaoDeSoliciticaoDeIngressoParaCurso(Usuario usuario, Curso curso) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioEnvio(usuario);
        notificacao.setCurso(curso);
        notificacao.setInstituicao(curso.getInstituicao());
        notificacao.setNotificacaoTipo(NotificacaoTipo.CURSO_INGRESSO_ALUNO);
        notificacao.setDescricao(usuario.getUrlPerfil() + " solicitou participar do curso " + curso.getUrlCurso() + " na instituição " + curso.getInstituicao().getUrlInstituicao() + ". Você aceita?");
        notificacao.setFinalizado(Boolean.FALSE);
        notificacaoDAO.save(notificacao);
    }

    public void gerarNotificacaoDeAceitacaoDeIngressoParaCurso(Notificacao notificacaoIngresso, Curso curso) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioNotificado(notificacaoIngresso.getUsuarioEnvio());
        notificacao.setCurso(curso);
        notificacao.setInstituicao(curso.getInstituicao());
        notificacao.setNotificacaoTipo(NotificacaoTipo.CURSO_INGRESSO_ACEITO);
        notificacao.setDescricao("Sua solicitação de ingresso no curso " + curso.getUrlCurso() + " na instituição " + curso.getInstituicao().getUrlInstituicao() + " foi aceita.");
        notificacao.setFinalizado(Boolean.FALSE);
        notificacao = notificacaoDAO.save(notificacao);
        notificacaoDAO.save(notificacao);
    }

    public void gerarNotificacaoDeConviteParaAva(Usuario usuario, Curso curso, CursoMembroAvaTipo tipo) {
        Notificacao notificacao = new Notificacao();
        notificacao.setUsuarioNotificado(usuario);
        notificacao.setCurso(curso);
        notificacao.setInstituicao(curso.getInstituicao());
        notificacao.setNotificacaoTipo(NotificacaoTipo.AVA_CONVITE);
        switch (tipo) {
            case PROFESSOR:
                notificacao.setDescricao("Você foi convidado para ser professor de AVA do curso " + curso.getNome() + " na instituição " + curso.getInstituicao().getNome() + ". Você aceita?");
                break;
            case ALUNO:
                notificacao.setDescricao("Você foi convidado para ser aluno de AVA do curso " + curso.getNome() + " na instituição " + curso.getInstituicao().getNome() + ". Você aceita?");
                break;
        }
        notificacao.setFinalizado(Boolean.FALSE);
        notificacao = notificacaoDAO.save(notificacao);
        notificacaoDAO.save(notificacao);

        CursoMembroAva cursoMembroAva = new CursoMembroAva();
        cursoMembroAva.setUsuario(usuario);
        cursoMembroAva.setCurso(curso);
        cursoMembroAva.setTipo(tipo);
        cursoMembroAva.setMoodleId(null);
        cursoMembroAva.setMoodlePassword(null);
        cursoMembroAva.setFinalizado(Boolean.FALSE);
        cursoMembroAvaDAO.save(cursoMembroAva);
    }

    private void gerarNotificacaoDeConviteAceitoParaAva(Notificacao notificacaoConvite) {
        String password = SenhaUtil.getRandomString(8);
        CursoMembroAva cursoMembro = buscarCursoMembro(notificacaoConvite.getUsuarioNotificado(), notificacaoConvite.getCurso());
        if (notificacaoConvite.getNotificacaoAceita()) {
            Object[] moodleUser = MoodleUtil.coreUserCreateUsers(notificacaoConvite.getUsuarioNotificado(), password, notificacaoConvite.getCurso().getInstituicao().getEnderecoAva(), notificacaoConvite.getCurso().getInstituicao().getTokenAcesso());
            for (Object atributo : moodleUser) {
                Integer moodleId = (Integer) ((HashMap) atributo).get("id");
                cursoMembro.setMoodleId(moodleId);
            }
            MoodleUtil.coreRoleAssignRoles(cursoMembro, notificacaoConvite.getCurso().getInstituicao().getEnderecoAva(), notificacaoConvite.getCurso().getInstituicao().getTokenAcesso());
            MoodleUtil.enrolManualEnrolUsers(cursoMembro, notificacaoConvite.getCurso().getInstituicao().getEnderecoAva(), notificacaoConvite.getCurso().getInstituicao().getTokenAcesso());
            Notificacao notificacao = new Notificacao();
            notificacao.setUsuarioNotificado(notificacaoConvite.getUsuarioNotificado());
            notificacao.setCurso(notificacaoConvite.getCurso());
            notificacao.setInstituicao(notificacaoConvite.getCurso().getInstituicao());
            notificacao.setNotificacaoTipo(NotificacaoTipo.AVA_CONVITE_ACEITO);
            notificacao.setDescricao("Você agora pode acessar o curso " + notificacaoConvite.getCurso().getNome() + " no AVA da instituição " + notificacaoConvite.getCurso().getInstituicao().getNome() + ", com o usuário " + notificacao.getUsuarioNotificado().getEmail() + " e senha " + password + ".");
            notificacao.setFinalizado(Boolean.TRUE);
            notificacao = notificacaoDAO.save(notificacao);
            notificacaoDAO.save(notificacao);
            cursoMembro.setFinalizado(Boolean.TRUE);
            cursoMembro.setMoodlePassword(password);
            cursoMembroAvaDAO.save(cursoMembro);
        } else {
            cursoMembroAvaDAO.remove(cursoMembro);
        }
    }

    public CursoMembroAva buscarCursoMembro(Usuario usuario, Curso curso) {
        List<CursoMembroAva> cursoMembros = new ArrayList<>();
        cursoMembros.addAll(cursoMembroAvaDAO.findAll());
        for (CursoMembroAva cursoMembro : cursoMembros) {
            if (cursoMembro.getCurso().getId().equals(curso.getId())
                    && cursoMembro.getUsuario().getId().equals(usuario.getId())) {
                return cursoMembro;
            }
        }
        return null;
    }

    public List<CursoMembroAva> buscarAvasDisponiveis(Usuario usuario) {
        String query = "SELECT cursoMembro FROM " + CursoMembroAva.class.getSimpleName() + " cursoMembro"
                + "      WHERE cursoMembro.usuario.id = " + usuario.getId()
                + "        AND finalizado = true";
        return cursoMembroAvaDAO.findByQuery(query);
    }

    public CursoMembroAva buscarAvaPorId(Long id) {
        return cursoMembroAvaDAO.findByPrimaryKey(id);
    }

    public void removerCursoMembroAva(Usuario usuario, Curso curso) {
        CursoMembroAva ava = buscarCursoMembro(usuario, curso);
        if (ava != null) {
            cursoMembroAvaDAO.remove(ava);
        }
    }
}

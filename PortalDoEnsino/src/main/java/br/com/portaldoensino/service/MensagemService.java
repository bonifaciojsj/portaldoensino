package br.com.portaldoensino.service;

import br.com.portaldoensino.dao.DAO;
import br.com.portaldoensino.entity.Mensagem;
import br.com.portaldoensino.entity.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author BONIFACIO
 */
@Stateless
public class MensagemService implements Serializable {

    @Inject
    private DAO<Mensagem, Long> mensagemDAO;

    public Mensagem salvar(Mensagem mensagem) {
        return mensagemDAO.save(mensagem);
    }

    public Mensagem buscarPorId(Long id) {
        return mensagemDAO.findByPrimaryKey(id);
    }

    public List<Mensagem> buscarTodosPorUsuario(Usuario usuario) {
        String query = "SELECT mensagem FROM " + Mensagem.class.getSimpleName() + " mensagem"
                + " WHERE mensagem.usuarioRecebimento.id = " + usuario.getId()
                + " OR mensagem.usuarioEnvio.id = " + usuario.getId();
        return mensagemDAO.findByQuery(query);
    }

    public List<Mensagem> buscarTodosNaoLidosPorUsuario(Usuario usuario) {
        String query = "SELECT mensagem FROM " + Mensagem.class.getSimpleName() + " mensagem"
                + " WHERE mensagem.usuarioRecebimento.id = " + usuario.getId()
                + " AND mensagem.visualizado = false";
        return mensagemDAO.findByQuery(query);
    }
    
    public List<Mensagem> buscarNaoLidosPorUsuarioEMensagem(Usuario usuario, Mensagem mensagem) {
        String query = "SELECT mensagem FROM " + Mensagem.class.getSimpleName() + " mensagem"
                + " WHERE mensagem.usuarioRecebimento.id = " + usuario.getId()
                + " AND mensagem.visualizado = false"
                + " AND mensagem.id = " + mensagem.getId();
        return mensagemDAO.findByQuery(query);
    }

    public List<Mensagem> buscarTodosDeMensagemPai(Mensagem mensagem) {
        String query = "SELECT mensagem FROM " + Mensagem.class.getSimpleName() + " mensagem"
                + " WHERE (mensagem.id = " + mensagem.getId() + " OR mensagem.mensagemPai.id = " + mensagem.getId() + ")"
                + " AND ((mensagem.usuarioEnvio.id = " + mensagem.getUsuarioEnvio().getId()
                + " OR mensagem.usuarioRecebimento.id = " + mensagem.getUsuarioEnvio().getId() + ")"
                + " OR (mensagem.usuarioEnvio.id = " + mensagem.getUsuarioRecebimento().getId()
                + " OR mensagem.usuarioRecebimento.id = " + mensagem.getUsuarioRecebimento().getId() + "))";
        return mensagemDAO.findByQuery(query);
    }

    public void atualizarMensagensNaoLidas(Mensagem mensagemPai, Usuario usuarioLogado) {
        for (Mensagem mensagem : buscarTodosDeMensagemPai(mensagemPai)) {
            if (!mensagem.getVisualizado() && mensagem.getUsuarioRecebimento().equals(usuarioLogado)) {
                mensagem.setVisualizado(Boolean.TRUE);
                salvar(mensagem);
            }
        }
    }

}

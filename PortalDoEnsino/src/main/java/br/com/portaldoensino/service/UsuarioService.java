package br.com.portaldoensino.service;

import br.com.portaldoensino.dao.DAO;
import br.com.portaldoensino.entity.Curso;
import br.com.portaldoensino.entity.Grupo;
import br.com.portaldoensino.entity.Instituicao;
import br.com.portaldoensino.entity.Notificacao;
import br.com.portaldoensino.entity.Usuario;
import br.com.portaldoensino.entity.UsuarioAmigo;
import br.com.portaldoensino.entity.UsuarioSeguidor;
import br.com.portaldoensino.exception.PortalDoEnsinoException;
import br.com.portaldoensino.util.SenhaUtil;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

/**
 *
 * @author BONIFACIO
 */
@Stateless
public class UsuarioService implements Serializable {

    @Inject
    private DAO<Usuario, Long> usuarioDAO;
    @Inject
    private DAO<UsuarioAmigo, Long> usuarioAmigoDAO;
    @Inject
    private DAO<UsuarioSeguidor, Long> usuarioSeguidorDAO;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Usuario salvar(Usuario usuario) {
        return usuarioDAO.save(usuario);
    }

    public void salvarDataVisualizacaoUltimaMensagem(UsuarioAmigo usuarioAmigo) {
        usuarioAmigo.setDataUltimaVisualizacao(new Date());
        usuarioAmigo.setMensagensNaoVisualizadas(0);
        usuarioAmigoDAO.save(usuarioAmigo);
    }

    public Usuario salvarNovoUsuario(Usuario usuario, String senhaConfirmacao) throws PortalDoEnsinoException {
        if (usuario.isNew()) {
            validarSenhaIgual(usuario.getSenha(), senhaConfirmacao);
        }
        return salvar(usuario);
    }

    public Usuario salvarUsuarioComNovaSenha(Usuario usuario,
            String senhaAntiga,
            String novaSenha,
            String novaSenhaConfirmacao) throws PortalDoEnsinoException {
        validarSenhaAntiga(usuario, senhaAntiga);
        validarSenhaIgual(novaSenha, novaSenhaConfirmacao);
        usuario.setSenhaCriptografada(novaSenha);
        return salvar(usuario);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void excluirAmizade(UsuarioAmigo amigo) {
        usuarioAmigoDAO.executeUpdate("DELETE FROM " + UsuarioAmigo.class.getSimpleName() + " where id = " + amigo.getId());
    }

    public Usuario buscarPorId(Long id) {
        return usuarioDAO.findByPrimaryKey(id);
    }

    public List<Usuario> buscarTodos() {
        return usuarioDAO.findAll();
    }
    
    public List<Usuario> buscarUsuariosPorNome(String nome) {
        String query = "SELECT DISTINCT usuario FROM " + Usuario.class.getSimpleName() + " usuario"
                + "      WHERE usuario.nome LIKE '%" + nome + "%'"
                + "         OR usuario.sobrenome LIKE '%" + nome + "%'";
        return usuarioDAO.findByQuery(query);
    }

    public List<Usuario> buscarUsuariosNaoMembrosENaoPendentesPorGrupo(Grupo grupo) {
        String query = "SELECT usuario FROM " + Usuario.class.getSimpleName() + " usuario"
                + " WHERE usuario NOT IN (SELECT membro FROM " + Grupo.class.getSimpleName() + " grupo"
                + "                    LEFT JOIN grupo.membros membro"
                + "                        WHERE grupo.id = " + grupo.getId() + ")"
                + " AND usuario NOT IN (SELECT usuarioNotificado FROM " + Notificacao.class.getSimpleName() + " notificacao"
                + "                  LEFT JOIN notificacao.usuarioNotificado usuarioNotificado"
                + "                      WHERE notificacao.finalizado = false"
                + "                        AND notificacao.notificacaoTipo = 'GRUPO_CONVITE_MEMBRO'"
                + "                        AND notificacao.grupo.id = " + grupo.getId() + ")";
        return usuarioDAO.findByQuery(query);
    }

    public List<Usuario> buscarUsuariosNaoAdministradoresENaoPendentesPorInstituicao(Instituicao instituicao) {
        String query = "SELECT usuario FROM " + Usuario.class.getSimpleName() + " usuario"
                + " WHERE usuario NOT IN (SELECT instituicao FROM " + Instituicao.class.getSimpleName() + " instituicao"
                + "                    LEFT JOIN instituicao.administradores administrador"
                + "                        WHERE instituicao.id = " + instituicao.getId() + ")"
                + " AND usuario NOT IN (SELECT usuarioNotificado FROM " + Notificacao.class.getSimpleName() + " notificacao"
                + "                  LEFT JOIN notificacao.usuarioNotificado usuarioNotificado"
                + "                      WHERE notificacao.finalizado = false"
                + "                        AND notificacao.notificacaoTipo = 'INSTITUICAO_CONVITE_ADMINITRADOR'"
                + "                        AND notificacao.instituicao.id = " + instituicao.getId() + ")";
        return usuarioDAO.findByQuery(query);
    }
    
    public List<Usuario> buscarUsuariosNaoAlunosENaoPendentesPorCurso(Curso curso) {
         String query = "SELECT usuario FROM " + Usuario.class.getSimpleName() + " usuario"
                + " WHERE usuario NOT IN (SELECT curso FROM " + Curso.class.getSimpleName() + " curso"
                + "                    LEFT JOIN curso.alunos aluno"
                + "                        WHERE curso.id = " + curso.getId() + ")"
                + " AND usuario NOT IN (SELECT usuarioNotificado FROM " + Notificacao.class.getSimpleName() + " notificacao"
                + "                  LEFT JOIN notificacao.usuarioNotificado usuarioNotificado"
                + "                      WHERE notificacao.finalizado = false"
                + "                        AND notificacao.notificacaoTipo = 'CURSO_CONVITE_ALUNO'"
                + "                        AND notificacao.curso.id = " + curso.getId() + ")";
        return usuarioDAO.findByQuery(query);       
    }

    public List<Usuario> buscarUsuariosNaoProfessoresENaoPendentesPorCurso(Curso curso) {
          String query = "SELECT usuario FROM " + Usuario.class.getSimpleName() + " usuario"
                + " WHERE usuario NOT IN (SELECT curso FROM " + Curso.class.getSimpleName() + " curso"
                + "                    LEFT JOIN curso.professores professor"
                + "                        WHERE curso.id = " + curso.getId() + ")"
                + " AND usuario NOT IN (SELECT usuarioNotificado FROM " + Notificacao.class.getSimpleName() + " notificacao"
                + "                  LEFT JOIN notificacao.usuarioNotificado usuarioNotificado"
                + "                      WHERE notificacao.finalizado = false"
                + "                        AND notificacao.notificacaoTipo = 'CURSO_CONVITE_PROFESSOR'"
                + "                        AND notificacao.curso.id = " + curso.getId() + ")";
        return usuarioDAO.findByQuery(query);   
    }
    
    public Usuario fetchTemas(Usuario usuario) {
        String query = "     SELECT usuario FROM " + Usuario.class.getSimpleName() + " usuario"
                + " LEFT JOIN FETCH usuario.temasFavoritos"
                + "           WHERE usuario.id = " + usuario.getId();
        return usuarioDAO.findByQuery(query).get(0);
    }

    public Usuario buscarPorNome(String nome) {
        String query = "SELECT usuario FROM " + Usuario.class.getSimpleName() + " usuario"
                + " WHERE usuario.nome LIKE '%" + nome + "%' OR usuario.sobrenome LIKE '%" + nome + "%'";
        List<Usuario> usuarios = usuarioDAO.findByQuery(query);
        if (!usuarios.isEmpty()) {
            return usuarios.get(0);
        }
        return null;
    }

    public UsuarioAmigo buscarPorUsuarios(Usuario usuario, Usuario amigo) {
        String query = "SELECT usuarioAmigo FROM " + UsuarioAmigo.class.getSimpleName() + " usuarioAmigo"
                + " WHERE usuarioAmigo.usuario.id = " + usuario.getId()
                + " AND usuarioAmigo.amigo.id = " + amigo.getId();
        List<UsuarioAmigo> usuarios = usuarioAmigoDAO.findByQuery(query);
        if (!usuarios.isEmpty()) {
            return usuarios.get(0);
        }
        return null;
    }

    public void seguirUsuario(Usuario usuarioLogado, Usuario usuarioSeguir) {
        UsuarioSeguidor usuarioSeguidor = new UsuarioSeguidor();
        usuarioSeguidor.setUsuario(usuarioLogado);
        usuarioSeguidor.setSeguidor(usuarioSeguir);
        usuarioSeguidorDAO.save(usuarioSeguidor);
    }

    public void deixarDeSeguirUsuario(Usuario usuarioLogado, Usuario usuarioSeguir) {
        UsuarioSeguidor usuarioSeguidor = buscarUsuarioSeguidorPorUsuarios(usuarioLogado, usuarioSeguir);
        usuarioAmigoDAO.executeUpdate("DELETE FROM " + UsuarioSeguidor.class.getSimpleName() + " where id = " + usuarioSeguidor.getId());
    }

    public UsuarioSeguidor buscarUsuarioSeguidorPorUsuarios(Usuario usuarioLogado, Usuario usuarioSeguir) {
        String query = "SELECT usuarioSeguidor FROM " + UsuarioSeguidor.class.getSimpleName() + " usuarioSeguidor"
                + " WHERE usuarioSeguidor.usuario.id = " + usuarioLogado.getId()
                + " AND usuarioSeguidor.seguidor.id = " + usuarioSeguir.getId();
        List<UsuarioSeguidor> usuarios = usuarioSeguidorDAO.findByQuery(query);
        if (!usuarios.isEmpty()) {
            return usuarios.get(0);
        }
        return null;
    }

    public Usuario buscarAtivosPorUsuarioESenha(String usario, String senha) {
        String query = "SELECT usuario FROM " + Usuario.class.getSimpleName()
                + " usuario WHERE usuario.email = '" + usario
                + "' AND usuario.senha = '" + SenhaUtil.getEncryptedPassword(senha) + "'"
                + " AND usuario.ativo = true";
        List<Usuario> usuarios = usuarioDAO.findByQuery(query);
        if (!usuarios.isEmpty()) {
            return usuarios.get(0);
        }
        return null;
    }

    public List<UsuarioAmigo> buscarAmigosPorUsuario(Usuario usuario) {
        String query = "SELECT DISTINCT usuarioAmigo FROM " + UsuarioAmigo.class.getSimpleName() + " usuarioAmigo"
                + " WHERE usuarioAmigo.usuario.id = " + usuario.getId();
        return usuarioAmigoDAO.findByQuery(query);
    }

    public void validarSenhaIgual(String senha1, String senha2) throws PortalDoEnsinoException {
        if (!senha1.equals(senha2)) {
            throw new PortalDoEnsinoException("A senha informada não coincide com sua confirmação.");
        }
    }

    public void validarSenhaAntiga(Usuario usuario, String senha) throws PortalDoEnsinoException {
        if (!usuario.getSenha().equals(senha)) {
            throw new PortalDoEnsinoException("A senha informada não coincide com a anterior.");
        }
    }
}

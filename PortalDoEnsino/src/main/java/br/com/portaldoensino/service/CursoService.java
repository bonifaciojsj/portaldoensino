package br.com.portaldoensino.service;

import br.com.portaldoensino.dao.DAO;
import br.com.portaldoensino.entity.Curso;
import br.com.portaldoensino.entity.Notificacao;
import br.com.portaldoensino.entity.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author BONIFACIO
 */
@Stateless
public class CursoService implements Serializable {

    @Inject
    private DAO<Curso, Long> cursoDAO;
    @Inject
    private DAO<Notificacao, Long> notificacaoDAO;

    public Curso salvar(Curso curso) {
        return cursoDAO.save(curso);
    }

    public Curso buscarPorId(Long id) {
        return cursoDAO.findByPrimaryKey(id);
    }

    public void removerCurso(Curso curso) {
        String query = "DELETE FROM " + Curso.class.getSimpleName() + " curso WHERE curso.id = " + curso.getId();
        cursoDAO.executeUpdate(query);
    }

    public List<Curso> buscarCursosParticipados(Usuario usuario) {
        String query = "SELECT DISTINCT curso FROM " + Curso.class.getSimpleName() + " curso"
                + " LEFT JOIN curso.professores professor"
                + " LEFT JOIN curso.alunos aluno"
                + " LEFT JOIN curso.instituicao instituicao"
                + " LEFT JOIN instituicao.administradores administrador"
                + "     WHERE professor.id = " + usuario.getId()
                + "          OR aluno.id = " + usuario.getId()
                + "          OR administrador.id = " + usuario.getId();
        return cursoDAO.findByQuery(query);
    }

    public boolean possuiSolicitacaoDeIngressoPendente(Usuario usuario, Curso curso) {
        String query = "SELECT notificacao FROM " + Notificacao.class.getSimpleName() + " notificacao"
                + " WHERE notificacao.curso.id = " + curso.getId()
                + " AND notificacao.finalizado = FALSE"
                + " AND (notificacao.notificacaoTipo = 'CURSO_CONVITE_ALUNO'"
                + " OR notificacao.notificacaoTipo = 'CURSO_INGRESSO_ALUNO')"
                + " AND (notificacao.usuarioEnvio.id = " + usuario.getId() + ""
                + " OR notificacao.usuarioNotificado.id = " + usuario.getId() + ")";
        return !notificacaoDAO.findByQuery(query).isEmpty();
    }

}

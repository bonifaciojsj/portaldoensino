package br.com.portaldoensino.service;

import br.com.portaldoensino.dao.DAO;
import br.com.portaldoensino.entity.Grupo;
import br.com.portaldoensino.entity.Notificacao;
import br.com.portaldoensino.entity.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author BONIFACIO
 */
@Stateless
public class GrupoService implements Serializable {
    
    @Inject
    private DAO<Grupo, Long> grupoDAO;
    @Inject
    private DAO<Notificacao, Long> notificacaoDAO;
    
    public Grupo salvar(Grupo grupo) {
        return grupoDAO.save(grupo);
    }
    
    public void excluir(Grupo grupo) {
        grupoDAO.remove(grupo);
    }
    
    public Grupo buscarPorId(Long id) {
        String query = "SELECT grupo FROM " + Grupo.class.getSimpleName() + " grupo"
                + " LEFT JOIN FETCH grupo.temas"
                + " WHERE grupo.id = " + id;
        return grupoDAO.findByQuery(query).get(0);
    }
    
    public List<Grupo> buscarGruposParticipados(Usuario usuario) {
        String query = "SELECT grupo FROM " + Grupo.class.getSimpleName() + " grupo"
                + " LEFT JOIN grupo.membros membro"
                + " WHERE membro.id = " + usuario.getId();
        return grupoDAO.findByQuery(query);
    }
    
    public List<Grupo> buscarPorString(String string) {
        String query = "SELECT DISTINCT grupo FROM " + Grupo.class.getSimpleName() + " grupo"
                + "  LEFT JOIN grupo.temas tema"
                + "      WHERE grupo.nome LIKE '%" + string + "%'"
                + "         OR tema.nome LIKE '%" + string + "%'";
        return grupoDAO.findByQuery(query);
    }
    
    public boolean possuiSolicitacaoDeIngressoPendente(Usuario usuario, Grupo grupo) {        
        String query = "SELECT notificacao FROM " + Notificacao.class.getSimpleName() + " notificacao"
                + " WHERE notificacao.grupo.id = " + grupo.getId()
                + " AND notificacao.finalizado = FALSE"
                + " AND (notificacao.notificacaoTipo = 'GRUPO_CONVITE_MEMBRO'"
                + " OR notificacao.notificacaoTipo = 'GRUPO_INGRESSO')"
                + " AND (notificacao.usuarioEnvio.id = " + usuario.getId() + ""
                + " OR notificacao.usuarioNotificado.id = " + usuario.getId() + ")";
        return !notificacaoDAO.findByQuery(query).isEmpty();
    }
    
    
    public boolean possuiConviteDeAdministradorPendente(Usuario usuario, Grupo grupo) {        
        String query = "SELECT notificacao FROM " + Notificacao.class.getSimpleName() + " notificacao"
                + " WHERE notificacao.grupo.id = " + grupo.getId()
                + " AND notificacao.finalizado = FALSE"
                + " AND notificacao.notificacaoTipo = 'GRUPO_CONVITE_ADMINISTRADOR'"
                + " AND (notificacao.usuarioEnvio.id = " + usuario.getId() + ""
                + " OR notificacao.usuarioNotificado.id = " + usuario.getId() + ")";
        return !notificacaoDAO.findByQuery(query).isEmpty();
    }
}

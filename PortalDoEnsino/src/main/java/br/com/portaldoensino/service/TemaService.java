package br.com.portaldoensino.service;

import br.com.portaldoensino.dao.DAO;
import br.com.portaldoensino.entity.Tema;
import br.com.portaldoensino.entity.TemaFavorito;
import br.com.portaldoensino.entity.TemaFavoritoPK;
import br.com.portaldoensino.entity.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Session;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class TemaService implements Serializable {

    @PersistenceContext
    private EntityManager em;
    @Inject
    private DAO<Tema, Long> temaDAO;
    @Inject
    private DAO<TemaFavorito, TemaFavoritoPK> temaAprovacaoDAO;

    public Tema salvar(Tema tema) {
        if (tema.isNew()) {
            tema.setNome(Character.toUpperCase(tema.getNome().charAt(0)) + tema.getNome().substring(1));
        }
        return temaDAO.save(tema);
    }

    public void excluir(Tema tema) {
        temaDAO.remove(tema);
    }

    public List<Tema> buscarTodos() {
        return temaDAO.findAll();
    }

    public Tema buscarPorId(Long id) {
        return temaDAO.findByPrimaryKey(id);
    }

    public void marcarTemaComoFavorito(Usuario usuario, Tema tema) {
        TemaFavoritoPK favoritoPK = new TemaFavoritoPK(usuario, tema);
        TemaFavorito favorito = new TemaFavorito();
        favorito.setId(favoritoPK);
        favorito.setUsuario(usuario);
        favorito.setTema(tema);
        temaAprovacaoDAO.save(favorito);
    }

    public void desmarcarTemaComoFavorito(Usuario usuario, Tema tema) {
        TemaFavoritoPK favoritoPK = new TemaFavoritoPK(usuario, tema);
        TemaFavorito favorito = new TemaFavorito();
        favorito.setId(favoritoPK);
        favorito.setUsuario(usuario);
        favorito.setTema(tema);
        temaAprovacaoDAO.remove(favorito);
    }

    public List<TemaFavorito> buscarTemasFavoritos(Usuario usuario) {
        String query = "SELECT temaFavorito FROM " + TemaFavorito.class.getSimpleName() + " temaFavorito"
                + " WHERE temaFavorito.usuario.id = " + usuario.getId();
        return temaAprovacaoDAO.findByQuery(query);
    }

    public Tema buscarPorNome(String nome) {
        String query = "SELECT tema FROM " + Tema.class.getSimpleName() + " tema"
                + " WHERE tema.nome = '" + nome + "'";
        List<Tema> temas = temaDAO.findByQuery(query);
        if (!temas.isEmpty()) {
            return temas.get(0);
        }
        return null;
    }

    public List<Tema> buscarTemasPorNome(String nome) {
        String query = "SELECT DISTINCT tema FROM " + Tema.class.getSimpleName() + " tema"
                + " WHERE tema.nome = '" + nome + "'";
        return temaDAO.findByQuery(query);
    }
}

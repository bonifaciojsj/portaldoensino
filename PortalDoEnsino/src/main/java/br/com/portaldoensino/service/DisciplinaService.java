package br.com.portaldoensino.service;

import br.com.portaldoensino.dao.DAO;
import br.com.portaldoensino.entity.Curso;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author BONIFACIO
 */
@Stateless
public class DisciplinaService implements Serializable {
    
    @Inject
    private DAO<Curso, Long> disciplinaDAO;
    
    public Curso salvar(Curso disciplina) {
        return disciplinaDAO.save(disciplina);
    }
    
    public void excluir(Curso disciplina) {
        disciplinaDAO.remove(disciplina);
    }
    
    public Curso buscarPorId(Long id) {
        return disciplinaDAO.findByPrimaryKey(id);
    }
    
} 

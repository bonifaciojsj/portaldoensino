package br.com.portaldoensino.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author jose.bonifacio
 */
public class DAOProducer implements Serializable {
    
    @PersistenceContext
    private EntityManager em;
    
    public DAOProducer() {  }
    
    @Dependent
    @Produces
    public <E, PK extends Serializable> DAOImpl<E, PK> create(InjectionPoint injectionPoint) {
        ParameterizedType type = (ParameterizedType) injectionPoint.getType();
        Class entityClass = (Class) type.getActualTypeArguments()[0];
        return new DAOImpl<>(em, entityClass);
    }
}

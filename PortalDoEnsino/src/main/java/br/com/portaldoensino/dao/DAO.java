package br.com.portaldoensino.dao;

import br.com.portaldoensino.entity.PersistentEntity;
import java.io.Serializable;
import java.util.List;


/**
 *
 * @author jose.bonifacio
 */
public interface DAO<E, PK> extends Serializable {
    
    public <E extends PersistentEntity> E save(E entity);
    public <E extends PersistentEntity> void remove(E entity);
    public E findByPrimaryKey(PK pk);
    public List<E> findAll();
    public List<E> findByQuery(String query);
    public void executeUpdate(String query);
}

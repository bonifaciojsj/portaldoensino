package br.com.portaldoensino.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 *
 * @author jose.bonifacio
 */
public class SenhaUtil {

    static final String NUMERIC = "0123456789";
    static final String LOWE_CASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static final String UPPER_CASE = "abcdefghijklmnopqrstuvxyz";
    static final String ALFA = "!@#$%";
    static Random rnd = new Random();

    public static String getRandomString(int len) {
        len = len / 4;
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(NUMERIC.charAt(rnd.nextInt(NUMERIC.length())));
        }
        for (int i = 0; i < len; i++) {
            sb.append(LOWE_CASE.charAt(rnd.nextInt(LOWE_CASE.length())));
        }
        for (int i = 0; i < len; i++) {
            sb.append(UPPER_CASE.charAt(rnd.nextInt(UPPER_CASE.length())));
        }
        for (int i = 0; i < len; i++) {
            sb.append(ALFA.charAt(rnd.nextInt(ALFA.length())));
        }
        return sb.toString();
    }

    public static String getEncryptedPassword(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes());

            byte byteData[] = md.digest();

            //convert the byte to hex format method 1
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            return sb.toString();

        } catch (NoSuchAlgorithmException ex) {
            ex.getLocalizedMessage();
        }
        return null;
    }
}

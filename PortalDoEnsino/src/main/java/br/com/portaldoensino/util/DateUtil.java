package br.com.portaldoensino.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author jose.bonifacio
 */
public class DateUtil {
    
    private final static DateFormat DATE_PATTERN = new SimpleDateFormat("dd/MM/yyyy");
    private final static DateFormat TIMESTAMP_PATTERN = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    
    public static String getFormattedDate(Date data) {
        return DATE_PATTERN.format(data);
    }
    
    public static String getFormattedTimestamp(Timestamp data) {
        return TIMESTAMP_PATTERN.format(data);
    }        
}

package br.com.portaldoensino.util;

import br.com.portaldoensino.entity.CursoMembroAva;
import br.com.portaldoensino.entity.CursoMembroAvaTipo;
import br.com.portaldoensino.entity.Usuario;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

/**
 *
 * @author BONIFACIO
 */
public class MoodleUtil {

    public static Object[] coreUserCreateUsers(Usuario usuario, String password, String enderecoMoodle, String token) {
        HashMap moodleUser = new HashMap<>();
        moodleUser.put("username", usuario.getEmail());
        moodleUser.put("password", password);
        moodleUser.put("firstname", usuario.getNome());
        moodleUser.put("lastname", usuario.getSobrenome());
        moodleUser.put("email", usuario.getEmail());
        return executeFunction("core_user_create_users", new Object[]{moodleUser}, enderecoMoodle, token);
    }

    public static Object[] coreRoleAssignRoles(CursoMembroAva membroAva, String enderecoMoodle, String token) {
        HashMap parameters = new HashMap();
        if (membroAva.getTipo().equals(CursoMembroAvaTipo.PROFESSOR)) {
            parameters.put("roleid", 3);
        } else {
            parameters.put("roleid", 5);
        }
        parameters.put("userid", membroAva.getMoodleId());
        parameters.put("contextid", 1);
        return executeFunction("core_role_assign_roles", new Object[]{parameters}, enderecoMoodle, token);
    }

    public static Object[] enrolManualEnrolUsers(CursoMembroAva membroAva, String enderecoMoodle, String token) {
        HashMap parameters = new HashMap();
        if (membroAva.getTipo().equals(CursoMembroAvaTipo.PROFESSOR)) {
            parameters.put("roleid", "3");
        } else {
            parameters.put("roleid", "5");
        }
        parameters.put("userid", membroAva.getMoodleId());
        parameters.put("courseid", Integer.valueOf(membroAva.getCurso().getIdentificadorCursoAva()));
        return executeFunction("enrol_manual_enrol_users", new Object[]{parameters}, enderecoMoodle, token);
    }

    private static Object[] executeFunction(String functionName, Object[] params, String enderecoMoodle, String token) {
        try {
            String url = enderecoMoodle + "/webservice/xmlrpc/server.php" + "?wstoken=" + token + "&wsfunction=" + functionName;
            XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
            config.setServerURL(new URL(url));
            XmlRpcClient client = new XmlRpcClient();
            client.setConfig(config);
            return (Object[]) client.execute(functionName, new Object[]{params});
        } catch (MalformedURLException | XmlRpcException ex) {
            System.out.println("teste: " + ex.getMessage());
            return null;
        }

    }
}

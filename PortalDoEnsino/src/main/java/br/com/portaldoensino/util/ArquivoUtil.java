package br.com.portaldoensino.util;

import java.io.*;

/**
 *
 * @author jose.bonifacio
 */
public class ArquivoUtil {

    private static final String CAMINHO_EM_DISCO = "C:\\PortalDoEnsino\\Arquivos";

    public static String salvarArquivo(String caminhoRelativo, String nomeArquivo, InputStream stream) {
        try {
            String caminhoCompletoArquivo = CAMINHO_EM_DISCO + caminhoRelativo + nomeArquivo;
            criarPastasNecessarias(CAMINHO_EM_DISCO + caminhoRelativo);
            OutputStream out = new FileOutputStream(new File(caminhoCompletoArquivo));

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = stream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            stream.close();
            out.flush();
            out.close();
            return caminhoCompletoArquivo;
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public static void criarPastasNecessarias(String caminhoPasta) {
        File file = new File(caminhoPasta);
        if (!file.exists()) {
            file.mkdirs();
        }
    }
}

Portal do Ensino is a educational social network software built using Java (JSF).

Project stack:

- Java;
- JSF;
- Hibernate;
- Glassfish;
- Mysql;
- PrimeFaces;

If you want to run this project please contact me at bonifacio.jsj@gmail.com and I will help you to setup the application correctly.